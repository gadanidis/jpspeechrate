/* globals jsPsych */

var timeline = [];
var my_id = jsPsych.randomization.randomID(8);
var should_i_save = 1;
var completed_trials = 0;

// welcome page
var welcome = {
    type: 'html-keyboard-response',
    stimulus: `本実験へようこそ。準備ができましたら、どれかキーを押してください。`
};

// Consent process

var agree = "わたしは上記「承諾書」の内容を十分に理解した上で、" +
    "本実験への参加を承諾します。";

var disagree = "わたしは上記「承諾書」の内容を十分に理解した上で、" +
    "本実験への参加を辞退します。";


var consent_options = [agree, disagree];

var consent = {
    type: 'survey-multi-choice',
    preamble: `
<div style='text-align: left;'>
<h1>言語学実験</h1>
<h2>研究プロジェクト参加承諾</h2>

<h3>参加要請とプロジェクト概要</h3>

<p>
    この承諾書は言語学実験への参加を要請するものです。
    ご希望であれば、参加を断ることも可能です。
    あなたの言語知識が研究員にとって科学的関心に値すると判断されました。
    この研究調査に参加するか否かを判断していただく上で、調査の目的、リスク、
    そして利益を知っていただく必要があります。
    この承諾書は以下に、実験の詳細な進め方についての情報を記しています。
    この研究の代表者はトロント大学のカン・ユンジュン（Yoonjung Kang）教授です。
    この研究は、様々な言語環境に応じて、
    日本語話者による音声の知覚の仕方がどのように変化するかを
    調査することを目的としています。
    実験の進め方を理解していただいた段階で、参加するか否かを決定してください。
</p>

<h3> 実験内容 </h3>
<p>
    この実験では、日本語母国語話者によって発せられる文章をいくつか聴いて頂き、聴こ
    えた音声に最も近い単語を選んで頂きます。
</p>

<h3>実験参加に伴う危険および影響</h3>
<p>
    この実験に伴う危険および影響等はございません。
</p>

<h3>利益</h3>
<p>
    本実験に参加することで、参加者の方々に個人的な利益は発生しませんが、実験を通し
    て収集された情報は、本研究の分野において将来的に有益になり得るものと考えており
    ます。
</p>

<h3>報酬</h3>
<p>
    実験に参加頂く報酬として、330円をお支払いします。
    尚、実験に要する時間はおよそ２０〜３０分です。
</p>

<h3>個人情報の保護</h3>
<p>
    この実験は匿名で行われます。
    参加頂く方のお名前が記録されることはありません。
    将来的に学会や出版物を通して研究結果が発表される場合でも、参加者のお名前は公表さ
    れません。
    この実験に際して収集したその他の個人情報は、実験に直接携わる担当研究
    員および共同研究員を除いては、開示することはありません。
    また、これらの個人情報
    は研究員によって可能な限り安全を考慮して保管され、研究結果の発表後、破棄される
    予定です。
    しかし、実験中に収集された参加者の回答などの情報は、参加者の同意のも
    と、匿名でその後も保管されます。
    これは、言語学の研究分野では標準的な慣習で、地
    域間や世代間の言語的変遷を調査する際に必要なデータとして使用する為です。
    また、参加される研究が、
    きちんと関連する法律やガイドラインに沿って行われているのを確認する目的で、
    個人情報を含めた研究に関する情報が大学の関係部署より検査される場合があります。
    この場合、参加者の個人情報は本研究チームが保護する上記の方法と同様の方
    法で保護されます。
</p>

<h3>任意参加</h3>
<p>
    本実験への参加は強制ではありません。あなたには本実験を辞退する権利があります。
    仮に参加を承諾して頂いた場合でも、実験の途中、いかなる段階においても、辞退する
    ことが可能です。
    また、仮に本実験への参加を辞退した場合でも、将来行われる別の実
    験への参加の権利等には影響はありません。
</p>

<h3>質問</h3>
<p>
    本承諾書に署名をして頂く前に、質問があれば研究員にお問い合わせください。
    尚、実験に影響を与えると考えられる質問への回答は差し控えさせて頂きます。
    必要であれば、充分に時間を取って頂き、本実験への参加するか否かのご判断をお願いいたします。
</p>

<h3>問い合わせ先</h3>
<p>
    本研究に関して質問等がある場合は、
    本プロジェクトの代表であるカン・ユンジュン教授（kang@utsc.utoronto.ca）
    までご連絡ください。
    また、本研究に際して、参加者であるご自身の権利等に関してご質問がある場合は、
    トロント大学のOffice of Research Ethics
    （研究倫理オフィス、416-946-3273またはethics.review@utoronto.ca）
    までお問い合わせください。
</p>
      `,
    questions: [{
        prompt: "<h3>承諾表明</h3>",
        options: consent_options,
        required: true
    }],
    error_message: "この項目は必須です。",
    button_label: "次へ",
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        jsPsych.data.addProperties({ consent: responses.Q0 });
    }
};

var no_consent = {
    type: 'html-keyboard-response',
    stimulus: "お時間をいただきありがとうございました。",
    on_finish: function(){
        if (true){
            should_i_save = 0;
            jsPsych.endExperiment("お時間をいただきありがとうございます。");
        }
    }
};

var check_consent = {
    timeline: [no_consent],
    conditional_function: function(){
        var data = jsPsych.data.getLastTrialData().values()[0];
        var responses = JSON.parse(data.responses);
        if (responses.Q0 === disagree){
            return true;
        } else {
            return false;
        }
    }
};

// Ask the participant for demo info and save it to data
var demographic_questions = {
    type: 'survey-text',
    button_label: "次へ",
    error_message: "この項目は必須です。",
    questions: [
        {prompt: "生まれた年", required: true},
        {prompt: "性別", required: true},
        {prompt: "出身の都道府県", required: true},
        {prompt: "話せる言語（日本語以外）", required: true}
    ],
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        var age = responses.Q0;
        var my_age = age;
        var gender = responses.Q1;
        var my_gender = gender;
        var origin = responses.Q2;
        var my_origin = origin;
        var languages = responses.Q3;
        var my_languages = languages;
        jsPsych.data.addProperties({
            subject_id: my_id,
            gender: my_gender,
            age: my_age,
            origin: my_origin,
            languages: my_languages}
        );
    }
};

// Instructions

var instructions = {
    type: 'html-keyboard-response',
    stimulus: "<p> この実験では、日本語話者によって読み上げられる「竹内さんはとても穏やかに◯◯を発音した」" +
        "という文の音声を聞き、〇〇に入る言葉を選んで頂きます。 </p>" +
        "<p> ０か１のキーを押して、竹内さんが発音した単語を選んでください。 </p>" +
        "<p> 準備ができたらスペース・バーを押して開始してください。 </p>",
    choices: [' ']
};


// Scales for test
var nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

var meta  = "１：メタ"   + nbsp + "０：滅多";
var tate  = "１：縦"     + nbsp + "０：立って";
var ika   = "１：以下"   + nbsp + "０：一家";
var kako  = "１：過去"   + nbsp + "０：括弧";

var kado  = "１：角"     + nbsp + "０：カード";
var biru  = "１：ビル"   + nbsp + "０：ビール";
var medo  = "１：目処"   + nbsp + "０：明度";
var tosho = "１：図書"   + nbsp + "０：当初";

// Test procedure
var trial = {
    type: 'audio-keyboard-response',
    choices: ['0', '1'],
    prompt: jsPsych.timelineVariable('opt'),
    stimulus: jsPsych.timelineVariable('audio'),
    on_finish: function(data){
        if (data.key_press === 48) {
            data.response = "long";
        } else {
            data.response = "short";
        }
        completed_trials += 1;
    }
};

var take_a_break = {
    type: 'html-keyboard-response',
    stimulus: `ちょっと休憩できます。
               続行する準備ができたら、
               スペースバーを押してください。`,
    choices: (' ')
};

var maybe_break = {
    timeline: [take_a_break],
    conditional_function: function(){
        if (completed_trials % 32 === 0) {
            return true;
        } else {
            return false;
        }
    }
};

var test_procedure = {
    timeline: [trial, maybe_break],
    timeline_variables: [
        { audio: '../stim/pitch/biiru_long_142.mp3',       opt: biru  , orig:'biiru'  , contour:'long'      , dur: 142 },
        { audio: '../stim/pitch/biiru_long_170.mp3',       opt: biru  , orig:'biiru'  , contour:'long'      , dur: 170 },
        { audio: '../stim/pitch/biiru_long_197.mp3',       opt: biru  , orig:'biiru'  , contour:'long'      , dur: 197 },
        { audio: '../stim/pitch/biiru_long_260.mp3',       opt: biru  , orig:'biiru'  , contour:'long'      , dur: 260 },
        { audio: '../stim/pitch/biiru_long_80.mp3',        opt: biru  , orig:'biiru'  , contour:'long'      , dur: 80 },
        { audio: '../stim/pitch/biiru_semilong_142.mp3',   opt: biru  , orig:'biiru'  , contour:'semilong'  , dur: 142 },
        { audio: '../stim/pitch/biiru_semilong_170.mp3',   opt: biru  , orig:'biiru'  , contour:'semilong'  , dur: 170 },
        { audio: '../stim/pitch/biiru_semilong_197.mp3',   opt: biru  , orig:'biiru'  , contour:'semilong'  , dur: 197 },
        { audio: '../stim/pitch/biiru_semilong_260.mp3',   opt: biru  , orig:'biiru'  , contour:'semilong'  , dur: 260 },
        { audio: '../stim/pitch/biiru_semilong_80.mp3',    opt: biru  , orig:'biiru'  , contour:'semilong'  , dur: 80 },
        { audio: '../stim/pitch/biiru_semishort_142.mp3',  opt: biru  , orig:'biiru'  , contour:'semishort' , dur: 142 },
        { audio: '../stim/pitch/biiru_semishort_170.mp3',  opt: biru  , orig:'biiru'  , contour:'semishort' , dur: 170 },
        { audio: '../stim/pitch/biiru_semishort_197.mp3',  opt: biru  , orig:'biiru'  , contour:'semishort' , dur: 197 },
        { audio: '../stim/pitch/biiru_semishort_260.mp3',  opt: biru  , orig:'biiru'  , contour:'semishort' , dur: 260 },
        { audio: '../stim/pitch/biiru_semishort_80.mp3',   opt: biru  , orig:'biiru'  , contour:'semishort' , dur: 80 },
        { audio: '../stim/pitch/biiru_short_142.mp3',      opt: biru  , orig:'biiru'  , contour:'short'     , dur: 142 },
        { audio: '../stim/pitch/biiru_short_170.mp3',      opt: biru  , orig:'biiru'  , contour:'short'     , dur: 170 },
        { audio: '../stim/pitch/biiru_short_197.mp3',      opt: biru  , orig:'biiru'  , contour:'short'     , dur: 197 },
        { audio: '../stim/pitch/biiru_short_260.mp3',      opt: biru  , orig:'biiru'  , contour:'short'     , dur: 260 },
        { audio: '../stim/pitch/biiru_short_80.mp3',       opt: biru  , orig:'biiru'  , contour:'short'     , dur: 80 },
        { audio: '../stim/pitch/biru_long_142.mp3',        opt: biru  , orig:'biru'   , contour:'long'      , dur: 142 },
        { audio: '../stim/pitch/biru_long_170.mp3',        opt: biru  , orig:'biru'   , contour:'long'      , dur: 170 },
        { audio: '../stim/pitch/biru_long_197.mp3',        opt: biru  , orig:'biru'   , contour:'long'      , dur: 197 },
        { audio: '../stim/pitch/biru_long_260.mp3',        opt: biru  , orig:'biru'   , contour:'long'      , dur: 260 },
        { audio: '../stim/pitch/biru_long_80.mp3',         opt: biru  , orig:'biru'   , contour:'long'      , dur: 80 },
        { audio: '../stim/pitch/biru_semilong_142.mp3',    opt: biru  , orig:'biru'   , contour:'semilong'  , dur: 142 },
        { audio: '../stim/pitch/biru_semilong_170.mp3',    opt: biru  , orig:'biru'   , contour:'semilong'  , dur: 170 },
        { audio: '../stim/pitch/biru_semilong_197.mp3',    opt: biru  , orig:'biru'   , contour:'semilong'  , dur: 197 },
        { audio: '../stim/pitch/biru_semilong_260.mp3',    opt: biru  , orig:'biru'   , contour:'semilong'  , dur: 260 },
        { audio: '../stim/pitch/biru_semilong_80.mp3',     opt: biru  , orig:'biru'   , contour:'semilong'  , dur: 80 },
        { audio: '../stim/pitch/biru_semishort_142.mp3',   opt: biru  , orig:'biru'   , contour:'semishort' , dur: 142 },
        { audio: '../stim/pitch/biru_semishort_170.mp3',   opt: biru  , orig:'biru'   , contour:'semishort' , dur: 170 },
        { audio: '../stim/pitch/biru_semishort_197.mp3',   opt: biru  , orig:'biru'   , contour:'semishort' , dur: 197 },
        { audio: '../stim/pitch/biru_semishort_260.mp3',   opt: biru  , orig:'biru'   , contour:'semishort' , dur: 260 },
        { audio: '../stim/pitch/biru_semishort_80.mp3',    opt: biru  , orig:'biru'   , contour:'semishort' , dur: 80 },
        { audio: '../stim/pitch/biru_short_142.mp3',       opt: biru  , orig:'biru'   , contour:'short'     , dur: 142 },
        { audio: '../stim/pitch/biru_short_170.mp3',       opt: biru  , orig:'biru'   , contour:'short'     , dur: 170 },
        { audio: '../stim/pitch/biru_short_197.mp3',       opt: biru  , orig:'biru'   , contour:'short'     , dur: 197 },
        { audio: '../stim/pitch/biru_short_260.mp3',       opt: biru  , orig:'biru'   , contour:'short'     , dur: 260 },
        { audio: '../stim/pitch/biru_short_80.mp3',        opt: biru  , orig:'biru'   , contour:'short'     , dur: 80 },
        { audio: '../stim/pitch/ika_long_110.mp3',         opt: ika   , orig:'ika'    , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/ika_long_127.mp3',         opt: ika   , orig:'ika'    , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/ika_long_145.mp3',         opt: ika   , orig:'ika'    , contour:'long'      , dur: 145 },
        { audio: '../stim/pitch/ika_long_184.mp3',         opt: ika   , orig:'ika'    , contour:'long'      , dur: 184 },
        { audio: '../stim/pitch/ika_long_70.mp3',          opt: ika   , orig:'ika'    , contour:'long'      , dur: 70 },
        { audio: '../stim/pitch/ika_semilong_110.mp3',     opt: ika   , orig:'ika'    , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/ika_semilong_127.mp3',     opt: ika   , orig:'ika'    , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/ika_semilong_145.mp3',     opt: ika   , orig:'ika'    , contour:'semilong'  , dur: 145 },
        { audio: '../stim/pitch/ika_semilong_184.mp3',     opt: ika   , orig:'ika'    , contour:'semilong'  , dur: 184 },
        { audio: '../stim/pitch/ika_semilong_70.mp3',      opt: ika   , orig:'ika'    , contour:'semilong'  , dur: 70 },
        { audio: '../stim/pitch/ika_semishort_110.mp3',    opt: ika   , orig:'ika'    , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/ika_semishort_127.mp3',    opt: ika   , orig:'ika'    , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/ika_semishort_145.mp3',    opt: ika   , orig:'ika'    , contour:'semishort' , dur: 145 },
        { audio: '../stim/pitch/ika_semishort_184.mp3',    opt: ika   , orig:'ika'    , contour:'semishort' , dur: 184 },
        { audio: '../stim/pitch/ika_semishort_70.mp3',     opt: ika   , orig:'ika'    , contour:'semishort' , dur: 70 },
        { audio: '../stim/pitch/ika_short_110.mp3',        opt: ika   , orig:'ika'    , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/ika_short_127.mp3',        opt: ika   , orig:'ika'    , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/ika_short_145.mp3',        opt: ika   , orig:'ika'    , contour:'short'     , dur: 145 },
        { audio: '../stim/pitch/ika_short_184.mp3',        opt: ika   , orig:'ika'    , contour:'short'     , dur: 184 },
        { audio: '../stim/pitch/ika_short_70.mp3',         opt: ika   , orig:'ika'    , contour:'short'     , dur: 70 },
        { audio: '../stim/pitch/ikka_long_110.mp3',        opt: ika   , orig:'ikka'   , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/ikka_long_127.mp3',        opt: ika   , orig:'ikka'   , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/ikka_long_145.mp3',        opt: ika   , orig:'ikka'   , contour:'long'      , dur: 145 },
        { audio: '../stim/pitch/ikka_long_184.mp3',        opt: ika   , orig:'ikka'   , contour:'long'      , dur: 184 },
        { audio: '../stim/pitch/ikka_long_70.mp3',         opt: ika   , orig:'ikka'   , contour:'long'      , dur: 70 },
        { audio: '../stim/pitch/ikka_semilong_110.mp3',    opt: ika   , orig:'ikka'   , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/ikka_semilong_127.mp3',    opt: ika   , orig:'ikka'   , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/ikka_semilong_145.mp3',    opt: ika   , orig:'ikka'   , contour:'semilong'  , dur: 145 },
        { audio: '../stim/pitch/ikka_semilong_184.mp3',    opt: ika   , orig:'ikka'   , contour:'semilong'  , dur: 184 },
        { audio: '../stim/pitch/ikka_semilong_70.mp3',     opt: ika   , orig:'ikka'   , contour:'semilong'  , dur: 70 },
        { audio: '../stim/pitch/ikka_semishort_110.mp3',   opt: ika   , orig:'ikka'   , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/ikka_semishort_127.mp3',   opt: ika   , orig:'ikka'   , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/ikka_semishort_145.mp3',   opt: ika   , orig:'ikka'   , contour:'semishort' , dur: 145 },
        { audio: '../stim/pitch/ikka_semishort_184.mp3',   opt: ika   , orig:'ikka'   , contour:'semishort' , dur: 184 },
        { audio: '../stim/pitch/ikka_semishort_70.mp3',    opt: ika   , orig:'ikka'   , contour:'semishort' , dur: 70 },
        { audio: '../stim/pitch/ikka_short_110.mp3',       opt: ika   , orig:'ikka'   , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/ikka_short_127.mp3',       opt: ika   , orig:'ikka'   , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/ikka_short_145.mp3',       opt: ika   , orig:'ikka'   , contour:'short'     , dur: 145 },
        { audio: '../stim/pitch/ikka_short_184.mp3',       opt: ika   , orig:'ikka'   , contour:'short'     , dur: 184 },
        { audio: '../stim/pitch/ikka_short_70.mp3',        opt: ika   , orig:'ikka'   , contour:'short'     , dur: 70 },
        { audio: '../stim/pitch/kaado_long_132.mp3',       opt: kado  , orig:'kaado'  , contour:'long'      , dur: 132 },
        { audio: '../stim/pitch/kaado_long_151.mp3',       opt: kado  , orig:'kaado'  , contour:'long'      , dur: 151 },
        { audio: '../stim/pitch/kaado_long_169.mp3',       opt: kado  , orig:'kaado'  , contour:'long'      , dur: 169 },
        { audio: '../stim/pitch/kaado_long_212.mp3',       opt: kado  , orig:'kaado'  , contour:'long'      , dur: 212 },
        { audio: '../stim/pitch/kaado_long_89.mp3',        opt: kado  , orig:'kaado'  , contour:'long'      , dur: 89 },
        { audio: '../stim/pitch/kaado_semilong_132.mp3',   opt: kado  , orig:'kaado'  , contour:'semilong'  , dur: 132 },
        { audio: '../stim/pitch/kaado_semilong_151.mp3',   opt: kado  , orig:'kaado'  , contour:'semilong'  , dur: 151 },
        { audio: '../stim/pitch/kaado_semilong_169.mp3',   opt: kado  , orig:'kaado'  , contour:'semilong'  , dur: 169 },
        { audio: '../stim/pitch/kaado_semilong_212.mp3',   opt: kado  , orig:'kaado'  , contour:'semilong'  , dur: 212 },
        { audio: '../stim/pitch/kaado_semilong_89.mp3',    opt: kado  , orig:'kaado'  , contour:'semilong'  , dur: 89 },
        { audio: '../stim/pitch/kaado_semishort_132.mp3',  opt: kado  , orig:'kaado'  , contour:'semishort' , dur: 132 },
        { audio: '../stim/pitch/kaado_semishort_151.mp3',  opt: kado  , orig:'kaado'  , contour:'semishort' , dur: 151 },
        { audio: '../stim/pitch/kaado_semishort_169.mp3',  opt: kado  , orig:'kaado'  , contour:'semishort' , dur: 169 },
        { audio: '../stim/pitch/kaado_semishort_212.mp3',  opt: kado  , orig:'kaado'  , contour:'semishort' , dur: 212 },
        { audio: '../stim/pitch/kaado_semishort_89.mp3',   opt: kado  , orig:'kaado'  , contour:'semishort' , dur: 89 },
        { audio: '../stim/pitch/kaado_short_132.mp3',      opt: kado  , orig:'kaado'  , contour:'short'     , dur: 132 },
        { audio: '../stim/pitch/kaado_short_151.mp3',      opt: kado  , orig:'kaado'  , contour:'short'     , dur: 151 },
        { audio: '../stim/pitch/kaado_short_169.mp3',      opt: kado  , orig:'kaado'  , contour:'short'     , dur: 169 },
        { audio: '../stim/pitch/kaado_short_212.mp3',      opt: kado  , orig:'kaado'  , contour:'short'     , dur: 212 },
        { audio: '../stim/pitch/kaado_short_89.mp3',       opt: kado  , orig:'kaado'  , contour:'short'     , dur: 89 },
        { audio: '../stim/pitch/kado_long_132.mp3',        opt: kado  , orig:'kado'   , contour:'long'      , dur: 132 },
        { audio: '../stim/pitch/kado_long_151.mp3',        opt: kado  , orig:'kado'   , contour:'long'      , dur: 151 },
        { audio: '../stim/pitch/kado_long_169.mp3',        opt: kado  , orig:'kado'   , contour:'long'      , dur: 169 },
        { audio: '../stim/pitch/kado_long_212.mp3',        opt: kado  , orig:'kado'   , contour:'long'      , dur: 212 },
        { audio: '../stim/pitch/kado_long_89.mp3',         opt: kado  , orig:'kado'   , contour:'long'      , dur: 89 },
        { audio: '../stim/pitch/kado_semilong_132.mp3',    opt: kado  , orig:'kado'   , contour:'semilong'  , dur: 132 },
        { audio: '../stim/pitch/kado_semilong_151.mp3',    opt: kado  , orig:'kado'   , contour:'semilong'  , dur: 151 },
        { audio: '../stim/pitch/kado_semilong_169.mp3',    opt: kado  , orig:'kado'   , contour:'semilong'  , dur: 169 },
        { audio: '../stim/pitch/kado_semilong_212.mp3',    opt: kado  , orig:'kado'   , contour:'semilong'  , dur: 212 },
        { audio: '../stim/pitch/kado_semilong_89.mp3',     opt: kado  , orig:'kado'   , contour:'semilong'  , dur: 89 },
        { audio: '../stim/pitch/kado_semishort_132.mp3',   opt: kado  , orig:'kado'   , contour:'semishort' , dur: 132 },
        { audio: '../stim/pitch/kado_semishort_151.mp3',   opt: kado  , orig:'kado'   , contour:'semishort' , dur: 151 },
        { audio: '../stim/pitch/kado_semishort_169.mp3',   opt: kado  , orig:'kado'   , contour:'semishort' , dur: 169 },
        { audio: '../stim/pitch/kado_semishort_212.mp3',   opt: kado  , orig:'kado'   , contour:'semishort' , dur: 212 },
        { audio: '../stim/pitch/kado_semishort_89.mp3',    opt: kado  , orig:'kado'   , contour:'semishort' , dur: 89 },
        { audio: '../stim/pitch/kado_short_132.mp3',       opt: kado  , orig:'kado'   , contour:'short'     , dur: 132 },
        { audio: '../stim/pitch/kado_short_151.mp3',       opt: kado  , orig:'kado'   , contour:'short'     , dur: 151 },
        { audio: '../stim/pitch/kado_short_169.mp3',       opt: kado  , orig:'kado'   , contour:'short'     , dur: 169 },
        { audio: '../stim/pitch/kado_short_212.mp3',       opt: kado  , orig:'kado'   , contour:'short'     , dur: 212 },
        { audio: '../stim/pitch/kado_short_89.mp3',        opt: kado  , orig:'kado'   , contour:'short'     , dur: 89 },
        { audio: '../stim/pitch/kakko_long_109.mp3',       opt: kako  , orig:'kakko'  , contour:'long'      , dur: 109 },
        { audio: '../stim/pitch/kakko_long_124.mp3',       opt: kako  , orig:'kakko'  , contour:'long'      , dur: 124 },
        { audio: '../stim/pitch/kakko_long_140.mp3',       opt: kako  , orig:'kakko'  , contour:'long'      , dur: 140 },
        { audio: '../stim/pitch/kakko_long_174.mp3',       opt: kako  , orig:'kakko'  , contour:'long'      , dur: 174 },
        { audio: '../stim/pitch/kakko_long_74.mp3',        opt: kako  , orig:'kakko'  , contour:'long'      , dur: 74 },
        { audio: '../stim/pitch/kakko_semilong_109.mp3',   opt: kako  , orig:'kakko'  , contour:'semilong'  , dur: 109 },
        { audio: '../stim/pitch/kakko_semilong_124.mp3',   opt: kako  , orig:'kakko'  , contour:'semilong'  , dur: 124 },
        { audio: '../stim/pitch/kakko_semilong_140.mp3',   opt: kako  , orig:'kakko'  , contour:'semilong'  , dur: 140 },
        { audio: '../stim/pitch/kakko_semilong_174.mp3',   opt: kako  , orig:'kakko'  , contour:'semilong'  , dur: 174 },
        { audio: '../stim/pitch/kakko_semilong_74.mp3',    opt: kako  , orig:'kakko'  , contour:'semilong'  , dur: 74 },
        { audio: '../stim/pitch/kakko_semishort_109.mp3',  opt: kako  , orig:'kakko'  , contour:'semishort' , dur: 109 },
        { audio: '../stim/pitch/kakko_semishort_124.mp3',  opt: kako  , orig:'kakko'  , contour:'semishort' , dur: 124 },
        { audio: '../stim/pitch/kakko_semishort_140.mp3',  opt: kako  , orig:'kakko'  , contour:'semishort' , dur: 140 },
        { audio: '../stim/pitch/kakko_semishort_174.mp3',  opt: kako  , orig:'kakko'  , contour:'semishort' , dur: 174 },
        { audio: '../stim/pitch/kakko_semishort_74.mp3',   opt: kako  , orig:'kakko'  , contour:'semishort' , dur: 74 },
        { audio: '../stim/pitch/kakko_short_109.mp3',      opt: kako  , orig:'kakko'  , contour:'short'     , dur: 109 },
        { audio: '../stim/pitch/kakko_short_124.mp3',      opt: kako  , orig:'kakko'  , contour:'short'     , dur: 124 },
        { audio: '../stim/pitch/kakko_short_140.mp3',      opt: kako  , orig:'kakko'  , contour:'short'     , dur: 140 },
        { audio: '../stim/pitch/kakko_short_174.mp3',      opt: kako  , orig:'kakko'  , contour:'short'     , dur: 174 },
        { audio: '../stim/pitch/kakko_short_74.mp3',       opt: kako  , orig:'kakko'  , contour:'short'     , dur: 74 },
        { audio: '../stim/pitch/kako_long_109.mp3',        opt: kako  , orig:'kako'   , contour:'long'      , dur: 109 },
        { audio: '../stim/pitch/kako_long_124.mp3',        opt: kako  , orig:'kako'   , contour:'long'      , dur: 124 },
        { audio: '../stim/pitch/kako_long_140.mp3',        opt: kako  , orig:'kako'   , contour:'long'      , dur: 140 },
        { audio: '../stim/pitch/kako_long_174.mp3',        opt: kako  , orig:'kako'   , contour:'long'      , dur: 174 },
        { audio: '../stim/pitch/kako_long_74.mp3',         opt: kako  , orig:'kako'   , contour:'long'      , dur: 74 },
        { audio: '../stim/pitch/kako_semilong_109.mp3',    opt: kako  , orig:'kako'   , contour:'semilong'  , dur: 109 },
        { audio: '../stim/pitch/kako_semilong_124.mp3',    opt: kako  , orig:'kako'   , contour:'semilong'  , dur: 124 },
        { audio: '../stim/pitch/kako_semilong_140.mp3',    opt: kako  , orig:'kako'   , contour:'semilong'  , dur: 140 },
        { audio: '../stim/pitch/kako_semilong_174.mp3',    opt: kako  , orig:'kako'   , contour:'semilong'  , dur: 174 },
        { audio: '../stim/pitch/kako_semilong_74.mp3',     opt: kako  , orig:'kako'   , contour:'semilong'  , dur: 74 },
        { audio: '../stim/pitch/kako_semishort_109.mp3',   opt: kako  , orig:'kako'   , contour:'semishort' , dur: 109 },
        { audio: '../stim/pitch/kako_semishort_124.mp3',   opt: kako  , orig:'kako'   , contour:'semishort' , dur: 124 },
        { audio: '../stim/pitch/kako_semishort_140.mp3',   opt: kako  , orig:'kako'   , contour:'semishort' , dur: 140 },
        { audio: '../stim/pitch/kako_semishort_174.mp3',   opt: kako  , orig:'kako'   , contour:'semishort' , dur: 174 },
        { audio: '../stim/pitch/kako_semishort_74.mp3',    opt: kako  , orig:'kako'   , contour:'semishort' , dur: 74 },
        { audio: '../stim/pitch/kako_short_109.mp3',       opt: kako  , orig:'kako'   , contour:'short'     , dur: 109 },
        { audio: '../stim/pitch/kako_short_124.mp3',       opt: kako  , orig:'kako'   , contour:'short'     , dur: 124 },
        { audio: '../stim/pitch/kako_short_140.mp3',       opt: kako  , orig:'kako'   , contour:'short'     , dur: 140 },
        { audio: '../stim/pitch/kako_short_174.mp3',       opt: kako  , orig:'kako'   , contour:'short'     , dur: 174 },
        { audio: '../stim/pitch/kako_short_74.mp3',        opt: kako  , orig:'kako'   , contour:'short'     , dur: 74 },
        { audio: '../stim/pitch/medo_long_148.mp3',        opt: medo  , orig:'medo'   , contour:'long'      , dur: 148 },
        { audio: '../stim/pitch/medo_long_175.mp3',        opt: medo  , orig:'medo'   , contour:'long'      , dur: 175 },
        { audio: '../stim/pitch/medo_long_201.mp3',        opt: medo  , orig:'medo'   , contour:'long'      , dur: 201 },
        { audio: '../stim/pitch/medo_long_261.mp3',        opt: medo  , orig:'medo'   , contour:'long'      , dur: 261 },
        { audio: '../stim/pitch/medo_long_89.mp3',         opt: medo  , orig:'medo'   , contour:'long'      , dur: 89 },
        { audio: '../stim/pitch/medo_semilong_148.mp3',    opt: medo  , orig:'medo'   , contour:'semilong'  , dur: 148 },
        { audio: '../stim/pitch/medo_semilong_175.mp3',    opt: medo  , orig:'medo'   , contour:'semilong'  , dur: 175 },
        { audio: '../stim/pitch/medo_semilong_201.mp3',    opt: medo  , orig:'medo'   , contour:'semilong'  , dur: 201 },
        { audio: '../stim/pitch/medo_semilong_261.mp3',    opt: medo  , orig:'medo'   , contour:'semilong'  , dur: 261 },
        { audio: '../stim/pitch/medo_semilong_89.mp3',     opt: medo  , orig:'medo'   , contour:'semilong'  , dur: 89 },
        { audio: '../stim/pitch/medo_semishort_148.mp3',   opt: medo  , orig:'medo'   , contour:'semishort' , dur: 148 },
        { audio: '../stim/pitch/medo_semishort_175.mp3',   opt: medo  , orig:'medo'   , contour:'semishort' , dur: 175 },
        { audio: '../stim/pitch/medo_semishort_201.mp3',   opt: medo  , orig:'medo'   , contour:'semishort' , dur: 201 },
        { audio: '../stim/pitch/medo_semishort_261.mp3',   opt: medo  , orig:'medo'   , contour:'semishort' , dur: 261 },
        { audio: '../stim/pitch/medo_semishort_89.mp3',    opt: medo  , orig:'medo'   , contour:'semishort' , dur: 89 },
        { audio: '../stim/pitch/medo_short_148.mp3',       opt: medo  , orig:'medo'   , contour:'short'     , dur: 148 },
        { audio: '../stim/pitch/medo_short_175.mp3',       opt: medo  , orig:'medo'   , contour:'short'     , dur: 175 },
        { audio: '../stim/pitch/medo_short_201.mp3',       opt: medo  , orig:'medo'   , contour:'short'     , dur: 201 },
        { audio: '../stim/pitch/medo_short_261.mp3',       opt: medo  , orig:'medo'   , contour:'short'     , dur: 261 },
        { audio: '../stim/pitch/medo_short_89.mp3',        opt: medo  , orig:'medo'   , contour:'short'     , dur: 89 },
        { audio: '../stim/pitch/meido_long_148.mp3',       opt: medo  , orig:'meido'  , contour:'long'      , dur: 148 },
        { audio: '../stim/pitch/meido_long_175.mp3',       opt: medo  , orig:'meido'  , contour:'long'      , dur: 175 },
        { audio: '../stim/pitch/meido_long_201.mp3',       opt: medo  , orig:'meido'  , contour:'long'      , dur: 201 },
        { audio: '../stim/pitch/meido_long_261.mp3',       opt: medo  , orig:'meido'  , contour:'long'      , dur: 261 },
        { audio: '../stim/pitch/meido_long_89.mp3',        opt: medo  , orig:'meido'  , contour:'long'      , dur: 89 },
        { audio: '../stim/pitch/meido_semilong_148.mp3',   opt: medo  , orig:'meido'  , contour:'semilong'  , dur: 148 },
        { audio: '../stim/pitch/meido_semilong_175.mp3',   opt: medo  , orig:'meido'  , contour:'semilong'  , dur: 175 },
        { audio: '../stim/pitch/meido_semilong_201.mp3',   opt: medo  , orig:'meido'  , contour:'semilong'  , dur: 201 },
        { audio: '../stim/pitch/meido_semilong_261.mp3',   opt: medo  , orig:'meido'  , contour:'semilong'  , dur: 261 },
        { audio: '../stim/pitch/meido_semilong_89.mp3',    opt: medo  , orig:'meido'  , contour:'semilong'  , dur: 89 },
        { audio: '../stim/pitch/meido_semishort_148.mp3',  opt: medo  , orig:'meido'  , contour:'semishort' , dur: 148 },
        { audio: '../stim/pitch/meido_semishort_175.mp3',  opt: medo  , orig:'meido'  , contour:'semishort' , dur: 175 },
        { audio: '../stim/pitch/meido_semishort_201.mp3',  opt: medo  , orig:'meido'  , contour:'semishort' , dur: 201 },
        { audio: '../stim/pitch/meido_semishort_261.mp3',  opt: medo  , orig:'meido'  , contour:'semishort' , dur: 261 },
        { audio: '../stim/pitch/meido_semishort_89.mp3',   opt: medo  , orig:'meido'  , contour:'semishort' , dur: 89 },
        { audio: '../stim/pitch/meido_short_148.mp3',      opt: medo  , orig:'meido'  , contour:'short'     , dur: 148 },
        { audio: '../stim/pitch/meido_short_175.mp3',      opt: medo  , orig:'meido'  , contour:'short'     , dur: 175 },
        { audio: '../stim/pitch/meido_short_201.mp3',      opt: medo  , orig:'meido'  , contour:'short'     , dur: 201 },
        { audio: '../stim/pitch/meido_short_261.mp3',      opt: medo  , orig:'meido'  , contour:'short'     , dur: 261 },
        { audio: '../stim/pitch/meido_short_89.mp3',       opt: medo  , orig:'meido'  , contour:'short'     , dur: 89 },
        { audio: '../stim/pitch/meta_long_110.mp3',        opt: meta  , orig:'meta'   , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/meta_long_129.mp3',        opt: meta  , orig:'meta'   , contour:'long'      , dur: 129 },
        { audio: '../stim/pitch/meta_long_148.mp3',        opt: meta  , orig:'meta'   , contour:'long'      , dur: 148 },
        { audio: '../stim/pitch/meta_long_191.mp3',        opt: meta  , orig:'meta'   , contour:'long'      , dur: 191 },
        { audio: '../stim/pitch/meta_long_66.mp3',         opt: meta  , orig:'meta'   , contour:'long'      , dur: 66 },
        { audio: '../stim/pitch/meta_semilong_110.mp3',    opt: meta  , orig:'meta'   , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/meta_semilong_129.mp3',    opt: meta  , orig:'meta'   , contour:'semilong'  , dur: 129 },
        { audio: '../stim/pitch/meta_semilong_148.mp3',    opt: meta  , orig:'meta'   , contour:'semilong'  , dur: 148 },
        { audio: '../stim/pitch/meta_semilong_191.mp3',    opt: meta  , orig:'meta'   , contour:'semilong'  , dur: 191 },
        { audio: '../stim/pitch/meta_semilong_66.mp3',     opt: meta  , orig:'meta'   , contour:'semilong'  , dur: 66 },
        { audio: '../stim/pitch/meta_semishort_110.mp3',   opt: meta  , orig:'meta'   , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/meta_semishort_129.mp3',   opt: meta  , orig:'meta'   , contour:'semishort' , dur: 129 },
        { audio: '../stim/pitch/meta_semishort_148.mp3',   opt: meta  , orig:'meta'   , contour:'semishort' , dur: 148 },
        { audio: '../stim/pitch/meta_semishort_191.mp3',   opt: meta  , orig:'meta'   , contour:'semishort' , dur: 191 },
        { audio: '../stim/pitch/meta_semishort_66.mp3',    opt: meta  , orig:'meta'   , contour:'semishort' , dur: 66 },
        { audio: '../stim/pitch/meta_short_110.mp3',       opt: meta  , orig:'meta'   , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/meta_short_129.mp3',       opt: meta  , orig:'meta'   , contour:'short'     , dur: 129 },
        { audio: '../stim/pitch/meta_short_148.mp3',       opt: meta  , orig:'meta'   , contour:'short'     , dur: 148 },
        { audio: '../stim/pitch/meta_short_191.mp3',       opt: meta  , orig:'meta'   , contour:'short'     , dur: 191 },
        { audio: '../stim/pitch/meta_short_66.mp3',        opt: meta  , orig:'meta'   , contour:'short'     , dur: 66 },
        { audio: '../stim/pitch/metta_long_110.mp3',       opt: meta  , orig:'metta'  , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/metta_long_129.mp3',       opt: meta  , orig:'metta'  , contour:'long'      , dur: 129 },
        { audio: '../stim/pitch/metta_long_148.mp3',       opt: meta  , orig:'metta'  , contour:'long'      , dur: 148 },
        { audio: '../stim/pitch/metta_long_191.mp3',       opt: meta  , orig:'metta'  , contour:'long'      , dur: 191 },
        { audio: '../stim/pitch/metta_long_66.mp3',        opt: meta  , orig:'metta'  , contour:'long'      , dur: 66 },
        { audio: '../stim/pitch/metta_semilong_110.mp3',   opt: meta  , orig:'metta'  , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/metta_semilong_129.mp3',   opt: meta  , orig:'metta'  , contour:'semilong'  , dur: 129 },
        { audio: '../stim/pitch/metta_semilong_148.mp3',   opt: meta  , orig:'metta'  , contour:'semilong'  , dur: 148 },
        { audio: '../stim/pitch/metta_semilong_191.mp3',   opt: meta  , orig:'metta'  , contour:'semilong'  , dur: 191 },
        { audio: '../stim/pitch/metta_semilong_66.mp3',    opt: meta  , orig:'metta'  , contour:'semilong'  , dur: 66 },
        { audio: '../stim/pitch/metta_semishort_110.mp3',  opt: meta  , orig:'metta'  , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/metta_semishort_129.mp3',  opt: meta  , orig:'metta'  , contour:'semishort' , dur: 129 },
        { audio: '../stim/pitch/metta_semishort_148.mp3',  opt: meta  , orig:'metta'  , contour:'semishort' , dur: 148 },
        { audio: '../stim/pitch/metta_semishort_191.mp3',  opt: meta  , orig:'metta'  , contour:'semishort' , dur: 191 },
        { audio: '../stim/pitch/metta_semishort_66.mp3',   opt: meta  , orig:'metta'  , contour:'semishort' , dur: 66 },
        { audio: '../stim/pitch/metta_short_110.mp3',      opt: meta  , orig:'metta'  , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/metta_short_129.mp3',      opt: meta  , orig:'metta'  , contour:'short'     , dur: 129 },
        { audio: '../stim/pitch/metta_short_148.mp3',      opt: meta  , orig:'metta'  , contour:'short'     , dur: 148 },
        { audio: '../stim/pitch/metta_short_191.mp3',      opt: meta  , orig:'metta'  , contour:'short'     , dur: 191 },
        { audio: '../stim/pitch/metta_short_66.mp3',       opt: meta  , orig:'metta'  , contour:'short'     , dur: 66 },
        { audio: '../stim/pitch/tate_long_110.mp3',       opt: tate  , orig:'tate'   , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/tate_long_127.mp3',       opt: tate  , orig:'tate'   , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/tate_long_144.mp3',       opt: tate  , orig:'tate'   , contour:'long'      , dur: 144 },
        { audio: '../stim/pitch/tate_long_183.mp3',       opt: tate  , orig:'tate'   , contour:'long'      , dur: 183 },
        { audio: '../stim/pitch/tate_long_72.mp3',        opt: tate  , orig:'tate'   , contour:'long'      , dur: 72 },
        { audio: '../stim/pitch/tate_semilong_110.mp3',   opt: tate  , orig:'tate'   , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/tate_semilong_127.mp3',   opt: tate  , orig:'tate'   , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/tate_semilong_144.mp3',   opt: tate  , orig:'tate'   , contour:'semilong'  , dur: 144 },
        { audio: '../stim/pitch/tate_semilong_183.mp3',   opt: tate  , orig:'tate'   , contour:'semilong'  , dur: 183 },
        { audio: '../stim/pitch/tate_semilong_72.mp3',    opt: tate  , orig:'tate'   , contour:'semilong'  , dur: 72 },
        { audio: '../stim/pitch/tate_semishort_110.mp3',  opt: tate  , orig:'tate'   , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/tate_semishort_127.mp3',  opt: tate  , orig:'tate'   , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/tate_semishort_144.mp3',  opt: tate  , orig:'tate'   , contour:'semishort' , dur: 144 },
        { audio: '../stim/pitch/tate_semishort_183.mp3',  opt: tate  , orig:'tate'   , contour:'semishort' , dur: 183 },
        { audio: '../stim/pitch/tate_semishort_72.mp3',   opt: tate  , orig:'tate'   , contour:'semishort' , dur: 72 },
        { audio: '../stim/pitch/tate_short_110.mp3',      opt: tate  , orig:'tate'   , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/tate_short_127.mp3',      opt: tate  , orig:'tate'   , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/tate_short_144.mp3',      opt: tate  , orig:'tate'   , contour:'short'     , dur: 144 },
        { audio: '../stim/pitch/tate_short_183.mp3',      opt: tate  , orig:'tate'   , contour:'short'     , dur: 183 },
        { audio: '../stim/pitch/tate_short_72.mp3',       opt: tate  , orig:'tate'   , contour:'short'     , dur: 72 },
        { audio: '../stim/pitch/tatte_long_110.mp3',      opt: tate  , orig:'tatte'  , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/tatte_long_127.mp3',      opt: tate  , orig:'tatte'  , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/tatte_long_144.mp3',      opt: tate  , orig:'tatte'  , contour:'long'      , dur: 144 },
        { audio: '../stim/pitch/tatte_long_183.mp3',      opt: tate  , orig:'tatte'  , contour:'long'      , dur: 183 },
        { audio: '../stim/pitch/tatte_long_72.mp3',       opt: tate  , orig:'tatte'  , contour:'long'      , dur: 72 },
        { audio: '../stim/pitch/tatte_semilong_110.mp3',  opt: tate  , orig:'tatte'  , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/tatte_semilong_127.mp3',  opt: tate  , orig:'tatte'  , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/tatte_semilong_144.mp3',  opt: tate  , orig:'tatte'  , contour:'semilong'  , dur: 144 },
        { audio: '../stim/pitch/tatte_semilong_183.mp3',  opt: tate  , orig:'tatte'  , contour:'semilong'  , dur: 183 },
        { audio: '../stim/pitch/tatte_semilong_72.mp3',   opt: tate  , orig:'tatte'  , contour:'semilong'  , dur: 72 },
        { audio: '../stim/pitch/tatte_semishort_110.mp3', opt: tate  , orig:'tatte'  , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/tatte_semishort_127.mp3', opt: tate  , orig:'tatte'  , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/tatte_semishort_144.mp3', opt: tate  , orig:'tatte'  , contour:'semishort' , dur: 144 },
        { audio: '../stim/pitch/tatte_semishort_183.mp3', opt: tate  , orig:'tatte'  , contour:'semishort' , dur: 183 },
        { audio: '../stim/pitch/tatte_semishort_72.mp3',  opt: tate  , orig:'tatte'  , contour:'semishort' , dur: 72 },
        { audio: '../stim/pitch/tatte_short_110.mp3',     opt: tate  , orig:'tatte'  , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/tatte_short_127.mp3',     opt: tate  , orig:'tatte'  , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/tatte_short_144.mp3',     opt: tate  , orig:'tatte'  , contour:'short'     , dur: 144 },
        { audio: '../stim/pitch/tatte_short_183.mp3',     opt: tate  , orig:'tatte'  , contour:'short'     , dur: 183 },
        { audio: '../stim/pitch/tatte_short_72.mp3',      opt: tate  , orig:'tatte'  , contour:'short'     , dur: 72 },
        { audio: '../stim/pitch/tosho_long_110.mp3',      opt: tosho , orig:'tosho'  , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/tosho_long_127.mp3',      opt: tosho , orig:'tosho'  , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/tosho_long_144.mp3',      opt: tosho , orig:'tosho'  , contour:'long'      , dur: 144 },
        { audio: '../stim/pitch/tosho_long_183.mp3',      opt: tosho , orig:'tosho'  , contour:'long'      , dur: 183 },
        { audio: '../stim/pitch/tosho_long_72.mp3',       opt: tosho , orig:'tosho'  , contour:'long'      , dur: 72 },
        { audio: '../stim/pitch/tosho_semilong_110.mp3',  opt: tosho , orig:'tosho'  , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/tosho_semilong_127.mp3',  opt: tosho , orig:'tosho'  , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/tosho_semilong_144.mp3',  opt: tosho , orig:'tosho'  , contour:'semilong'  , dur: 144 },
        { audio: '../stim/pitch/tosho_semilong_183.mp3',  opt: tosho , orig:'tosho'  , contour:'semilong'  , dur: 183 },
        { audio: '../stim/pitch/tosho_semilong_72.mp3',   opt: tosho , orig:'tosho'  , contour:'semilong'  , dur: 72 },
        { audio: '../stim/pitch/tosho_semishort_110.mp3', opt: tosho , orig:'tosho'  , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/tosho_semishort_127.mp3', opt: tosho , orig:'tosho'  , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/tosho_semishort_144.mp3', opt: tosho , orig:'tosho'  , contour:'semishort' , dur: 144 },
        { audio: '../stim/pitch/tosho_semishort_183.mp3', opt: tosho , orig:'tosho'  , contour:'semishort' , dur: 183 },
        { audio: '../stim/pitch/tosho_semishort_72.mp3',  opt: tosho , orig:'tosho'  , contour:'semishort' , dur: 72 },
        { audio: '../stim/pitch/tosho_short_110.mp3',     opt: tosho , orig:'tosho'  , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/tosho_short_127.mp3',     opt: tosho , orig:'tosho'  , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/tosho_short_144.mp3',     opt: tosho , orig:'tosho'  , contour:'short'     , dur: 144 },
        { audio: '../stim/pitch/tosho_short_183.mp3',     opt: tosho , orig:'tosho'  , contour:'short'     , dur: 183 },
        { audio: '../stim/pitch/tosho_short_72.mp3',      opt: tosho , orig:'tosho'  , contour:'short'     , dur: 72 },
        { audio: '../stim/pitch/tousho_long_110.mp3',     opt: tosho , orig:'tousho' , contour:'long'      , dur: 110 },
        { audio: '../stim/pitch/tousho_long_127.mp3',     opt: tosho , orig:'tousho' , contour:'long'      , dur: 127 },
        { audio: '../stim/pitch/tousho_long_144.mp3',     opt: tosho , orig:'tousho' , contour:'long'      , dur: 144 },
        { audio: '../stim/pitch/tousho_long_183.mp3',     opt: tosho , orig:'tousho' , contour:'long'      , dur: 183 },
        { audio: '../stim/pitch/tousho_long_72.mp3',      opt: tosho , orig:'tousho' , contour:'long'      , dur: 72 },
        { audio: '../stim/pitch/tousho_semilong_110.mp3', opt: tosho , orig:'tousho' , contour:'semilong'  , dur: 110 },
        { audio: '../stim/pitch/tousho_semilong_127.mp3', opt: tosho , orig:'tousho' , contour:'semilong'  , dur: 127 },
        { audio: '../stim/pitch/tousho_semilong_144.mp3', opt: tosho , orig:'tousho' , contour:'semilong'  , dur: 144 },
        { audio: '../stim/pitch/tousho_semilong_183.mp3', opt: tosho , orig:'tousho' , contour:'semilong'  , dur: 183 },
        { audio: '../stim/pitch/tousho_semilong_72.mp3',  opt: tosho , orig:'tousho' , contour:'semilong'  , dur: 72 },
        { audio: '../stim/pitch/tousho_semishort_110.mp3',opt: tosho , orig:'tousho' , contour:'semishort' , dur: 110 },
        { audio: '../stim/pitch/tousho_semishort_127.mp3',opt: tosho , orig:'tousho' , contour:'semishort' , dur: 127 },
        { audio: '../stim/pitch/tousho_semishort_144.mp3',opt: tosho , orig:'tousho' , contour:'semishort' , dur: 144 },
        { audio: '../stim/pitch/tousho_semishort_183.mp3',opt: tosho , orig:'tousho' , contour:'semishort' , dur: 183 },
        { audio: '../stim/pitch/tousho_semishort_72.mp3', opt: tosho , orig:'tousho' , contour:'semishort' , dur: 72 },
        { audio: '../stim/pitch/tousho_short_110.mp3',    opt: tosho , orig:'tousho' , contour:'short'     , dur: 110 },
        { audio: '../stim/pitch/tousho_short_127.mp3',    opt: tosho , orig:'tousho' , contour:'short'     , dur: 127 },
        { audio: '../stim/pitch/tousho_short_144.mp3',    opt: tosho , orig:'tousho' , contour:'short'     , dur: 144 },
        { audio: '../stim/pitch/tousho_short_183.mp3',    opt: tosho , orig:'tousho' , contour:'short'     , dur: 183 },
        { audio: '../stim/pitch/tousho_short_72.mp3',     opt: tosho , orig:'tousho' , contour:'short'     , dur: 72 }
    ],
    randomize_order: true,
    data: {
        file: jsPsych.timelineVariable('audio'),
        duration: jsPsych.timelineVariable('dur'),
        original_word: jsPsych.timelineVariable('orig'),
        contour: jsPsych.timelineVariable('contour')
    }
};

// ending page

var ending = {
    type: 'html-keyboard-response',
    stimulus: "<p>実験に参加して下さってありがとうございます。</p>" +
    "<p>データを保存するにはスペースバーを押して下さい。</p>" +
    "<p>データが保存されたら、認証コードが表示されます。",
    response_ends_trial: true,
    choices: [' '],
    on_finish: function(){
        jsPsych.endExperiment(
            "<p>データが保存されました。</p>" +
            "<p> 認証コードは「" +
            my_id +
            "」です。そのコードをクラウドワークスに入力してください。</p>" +
            "<p>コードを忘れないように書き留めてください。</p>" +
            "<p>コードを書き留めたら、このウィンドーを閉じることができます。"
        );
    }
};

// timeline

var timeline = [];
timeline.push(welcome);
timeline.push(consent);
timeline.push(check_consent);
timeline.push(demographic_questions);
timeline.push(instructions);
timeline.push(test_procedure);
timeline.push(ending);

// preload audio

var audiopaths = [
    '../stim/pitch/biiru_long_142.mp3',
    '../stim/pitch/biiru_long_170.mp3',
    '../stim/pitch/biiru_long_197.mp3',
    '../stim/pitch/biiru_long_260.mp3',
    '../stim/pitch/biiru_long_80.mp3',
    '../stim/pitch/biiru_semilong_142.mp3',
    '../stim/pitch/biiru_semilong_170.mp3',
    '../stim/pitch/biiru_semilong_197.mp3',
    '../stim/pitch/biiru_semilong_260.mp3',
    '../stim/pitch/biiru_semilong_80.mp3',
    '../stim/pitch/biiru_semishort_142.mp3',
    '../stim/pitch/biiru_semishort_170.mp3',
    '../stim/pitch/biiru_semishort_197.mp3',
    '../stim/pitch/biiru_semishort_260.mp3',
    '../stim/pitch/biiru_semishort_80.mp3',
    '../stim/pitch/biiru_short_142.mp3',
    '../stim/pitch/biiru_short_170.mp3',
    '../stim/pitch/biiru_short_197.mp3',
    '../stim/pitch/biiru_short_260.mp3',
    '../stim/pitch/biiru_short_80.mp3',
    '../stim/pitch/biru_long_142.mp3',
    '../stim/pitch/biru_long_170.mp3',
    '../stim/pitch/biru_long_197.mp3',
    '../stim/pitch/biru_long_260.mp3',
    '../stim/pitch/biru_long_80.mp3',
    '../stim/pitch/biru_semilong_142.mp3',
    '../stim/pitch/biru_semilong_170.mp3',
    '../stim/pitch/biru_semilong_197.mp3',
    '../stim/pitch/biru_semilong_260.mp3',
    '../stim/pitch/biru_semilong_80.mp3',
    '../stim/pitch/biru_semishort_142.mp3',
    '../stim/pitch/biru_semishort_170.mp3',
    '../stim/pitch/biru_semishort_197.mp3',
    '../stim/pitch/biru_semishort_260.mp3',
    '../stim/pitch/biru_semishort_80.mp3',
    '../stim/pitch/biru_short_142.mp3',
    '../stim/pitch/biru_short_170.mp3',
    '../stim/pitch/biru_short_197.mp3',
    '../stim/pitch/biru_short_260.mp3',
    '../stim/pitch/biru_short_80.mp3',
    '../stim/pitch/ika_long_110.mp3',
    '../stim/pitch/ika_long_127.mp3',
    '../stim/pitch/ika_long_145.mp3',
    '../stim/pitch/ika_long_184.mp3',
    '../stim/pitch/ika_long_70.mp3',
    '../stim/pitch/ika_semilong_110.mp3',
    '../stim/pitch/ika_semilong_127.mp3',
    '../stim/pitch/ika_semilong_145.mp3',
    '../stim/pitch/ika_semilong_184.mp3',
    '../stim/pitch/ika_semilong_70.mp3',
    '../stim/pitch/ika_semishort_110.mp3',
    '../stim/pitch/ika_semishort_127.mp3',
    '../stim/pitch/ika_semishort_145.mp3',
    '../stim/pitch/ika_semishort_184.mp3',
    '../stim/pitch/ika_semishort_70.mp3',
    '../stim/pitch/ika_short_110.mp3',
    '../stim/pitch/ika_short_127.mp3',
    '../stim/pitch/ika_short_145.mp3',
    '../stim/pitch/ika_short_184.mp3',
    '../stim/pitch/ika_short_70.mp3',
    '../stim/pitch/ikka_long_110.mp3',
    '../stim/pitch/ikka_long_127.mp3',
    '../stim/pitch/ikka_long_145.mp3',
    '../stim/pitch/ikka_long_184.mp3',
    '../stim/pitch/ikka_long_70.mp3',
    '../stim/pitch/ikka_semilong_110.mp3',
    '../stim/pitch/ikka_semilong_127.mp3',
    '../stim/pitch/ikka_semilong_145.mp3',
    '../stim/pitch/ikka_semilong_184.mp3',
    '../stim/pitch/ikka_semilong_70.mp3',
    '../stim/pitch/ikka_semishort_110.mp3',
    '../stim/pitch/ikka_semishort_127.mp3',
    '../stim/pitch/ikka_semishort_145.mp3',
    '../stim/pitch/ikka_semishort_184.mp3',
    '../stim/pitch/ikka_semishort_70.mp3',
    '../stim/pitch/ikka_short_110.mp3',
    '../stim/pitch/ikka_short_127.mp3',
    '../stim/pitch/ikka_short_145.mp3',
    '../stim/pitch/ikka_short_184.mp3',
    '../stim/pitch/ikka_short_70.mp3',
    '../stim/pitch/kaado_long_132.mp3',
    '../stim/pitch/kaado_long_151.mp3',
    '../stim/pitch/kaado_long_169.mp3',
    '../stim/pitch/kaado_long_212.mp3',
    '../stim/pitch/kaado_long_89.mp3',
    '../stim/pitch/kaado_semilong_132.mp3',
    '../stim/pitch/kaado_semilong_151.mp3',
    '../stim/pitch/kaado_semilong_169.mp3',
    '../stim/pitch/kaado_semilong_212.mp3',
    '../stim/pitch/kaado_semilong_89.mp3',
    '../stim/pitch/kaado_semishort_132.mp3',
    '../stim/pitch/kaado_semishort_151.mp3',
    '../stim/pitch/kaado_semishort_169.mp3',
    '../stim/pitch/kaado_semishort_212.mp3',
    '../stim/pitch/kaado_semishort_89.mp3',
    '../stim/pitch/kaado_short_132.mp3',
    '../stim/pitch/kaado_short_151.mp3',
    '../stim/pitch/kaado_short_169.mp3',
    '../stim/pitch/kaado_short_212.mp3',
    '../stim/pitch/kaado_short_89.mp3',
    '../stim/pitch/kado_long_132.mp3',
    '../stim/pitch/kado_long_151.mp3',
    '../stim/pitch/kado_long_169.mp3',
    '../stim/pitch/kado_long_212.mp3',
    '../stim/pitch/kado_long_89.mp3',
    '../stim/pitch/kado_semilong_132.mp3',
    '../stim/pitch/kado_semilong_151.mp3',
    '../stim/pitch/kado_semilong_169.mp3',
    '../stim/pitch/kado_semilong_212.mp3',
    '../stim/pitch/kado_semilong_89.mp3',
    '../stim/pitch/kado_semishort_132.mp3',
    '../stim/pitch/kado_semishort_151.mp3',
    '../stim/pitch/kado_semishort_169.mp3',
    '../stim/pitch/kado_semishort_212.mp3',
    '../stim/pitch/kado_semishort_89.mp3',
    '../stim/pitch/kado_short_132.mp3',
    '../stim/pitch/kado_short_151.mp3',
    '../stim/pitch/kado_short_169.mp3',
    '../stim/pitch/kado_short_212.mp3',
    '../stim/pitch/kado_short_89.mp3',
    '../stim/pitch/kakko_long_109.mp3',
    '../stim/pitch/kakko_long_124.mp3',
    '../stim/pitch/kakko_long_140.mp3',
    '../stim/pitch/kakko_long_174.mp3',
    '../stim/pitch/kakko_long_74.mp3',
    '../stim/pitch/kakko_semilong_109.mp3',
    '../stim/pitch/kakko_semilong_124.mp3',
    '../stim/pitch/kakko_semilong_140.mp3',
    '../stim/pitch/kakko_semilong_174.mp3',
    '../stim/pitch/kakko_semilong_74.mp3',
    '../stim/pitch/kakko_semishort_109.mp3',
    '../stim/pitch/kakko_semishort_124.mp3',
    '../stim/pitch/kakko_semishort_140.mp3',
    '../stim/pitch/kakko_semishort_174.mp3',
    '../stim/pitch/kakko_semishort_74.mp3',
    '../stim/pitch/kakko_short_109.mp3',
    '../stim/pitch/kakko_short_124.mp3',
    '../stim/pitch/kakko_short_140.mp3',
    '../stim/pitch/kakko_short_174.mp3',
    '../stim/pitch/kakko_short_74.mp3',
    '../stim/pitch/kako_long_109.mp3',
    '../stim/pitch/kako_long_124.mp3',
    '../stim/pitch/kako_long_140.mp3',
    '../stim/pitch/kako_long_174.mp3',
    '../stim/pitch/kako_long_74.mp3',
    '../stim/pitch/kako_semilong_109.mp3',
    '../stim/pitch/kako_semilong_124.mp3',
    '../stim/pitch/kako_semilong_140.mp3',
    '../stim/pitch/kako_semilong_174.mp3',
    '../stim/pitch/kako_semilong_74.mp3',
    '../stim/pitch/kako_semishort_109.mp3',
    '../stim/pitch/kako_semishort_124.mp3',
    '../stim/pitch/kako_semishort_140.mp3',
    '../stim/pitch/kako_semishort_174.mp3',
    '../stim/pitch/kako_semishort_74.mp3',
    '../stim/pitch/kako_short_109.mp3',
    '../stim/pitch/kako_short_124.mp3',
    '../stim/pitch/kako_short_140.mp3',
    '../stim/pitch/kako_short_174.mp3',
    '../stim/pitch/kako_short_74.mp3',
    '../stim/pitch/medo_long_148.mp3',
    '../stim/pitch/medo_long_175.mp3',
    '../stim/pitch/medo_long_201.mp3',
    '../stim/pitch/medo_long_261.mp3',
    '../stim/pitch/medo_long_89.mp3',
    '../stim/pitch/medo_semilong_148.mp3',
    '../stim/pitch/medo_semilong_175.mp3',
    '../stim/pitch/medo_semilong_201.mp3',
    '../stim/pitch/medo_semilong_261.mp3',
    '../stim/pitch/medo_semilong_89.mp3',
    '../stim/pitch/medo_semishort_148.mp3',
    '../stim/pitch/medo_semishort_175.mp3',
    '../stim/pitch/medo_semishort_201.mp3',
    '../stim/pitch/medo_semishort_261.mp3',
    '../stim/pitch/medo_semishort_89.mp3',
    '../stim/pitch/medo_short_148.mp3',
    '../stim/pitch/medo_short_175.mp3',
    '../stim/pitch/medo_short_201.mp3',
    '../stim/pitch/medo_short_261.mp3',
    '../stim/pitch/medo_short_89.mp3',
    '../stim/pitch/meido_long_148.mp3',
    '../stim/pitch/meido_long_175.mp3',
    '../stim/pitch/meido_long_201.mp3',
    '../stim/pitch/meido_long_261.mp3',
    '../stim/pitch/meido_long_89.mp3',
    '../stim/pitch/meido_semilong_148.mp3',
    '../stim/pitch/meido_semilong_175.mp3',
    '../stim/pitch/meido_semilong_201.mp3',
    '../stim/pitch/meido_semilong_261.mp3',
    '../stim/pitch/meido_semilong_89.mp3',
    '../stim/pitch/meido_semishort_148.mp3',
    '../stim/pitch/meido_semishort_175.mp3',
    '../stim/pitch/meido_semishort_201.mp3',
    '../stim/pitch/meido_semishort_261.mp3',
    '../stim/pitch/meido_semishort_89.mp3',
    '../stim/pitch/meido_short_148.mp3',
    '../stim/pitch/meido_short_175.mp3',
    '../stim/pitch/meido_short_201.mp3',
    '../stim/pitch/meido_short_261.mp3',
    '../stim/pitch/meido_short_89.mp3',
    '../stim/pitch/meta_long_110.mp3',
    '../stim/pitch/meta_long_129.mp3',
    '../stim/pitch/meta_long_148.mp3',
    '../stim/pitch/meta_long_191.mp3',
    '../stim/pitch/meta_long_66.mp3',
    '../stim/pitch/meta_semilong_110.mp3',
    '../stim/pitch/meta_semilong_129.mp3',
    '../stim/pitch/meta_semilong_148.mp3',
    '../stim/pitch/meta_semilong_191.mp3',
    '../stim/pitch/meta_semilong_66.mp3',
    '../stim/pitch/meta_semishort_110.mp3',
    '../stim/pitch/meta_semishort_129.mp3',
    '../stim/pitch/meta_semishort_148.mp3',
    '../stim/pitch/meta_semishort_191.mp3',
    '../stim/pitch/meta_semishort_66.mp3',
    '../stim/pitch/meta_short_110.mp3',
    '../stim/pitch/meta_short_129.mp3',
    '../stim/pitch/meta_short_148.mp3',
    '../stim/pitch/meta_short_191.mp3',
    '../stim/pitch/meta_short_66.mp3',
    '../stim/pitch/metta_long_110.mp3',
    '../stim/pitch/metta_long_129.mp3',
    '../stim/pitch/metta_long_148.mp3',
    '../stim/pitch/metta_long_191.mp3',
    '../stim/pitch/metta_long_66.mp3',
    '../stim/pitch/metta_semilong_110.mp3',
    '../stim/pitch/metta_semilong_129.mp3',
    '../stim/pitch/metta_semilong_148.mp3',
    '../stim/pitch/metta_semilong_191.mp3',
    '../stim/pitch/metta_semilong_66.mp3',
    '../stim/pitch/metta_semishort_110.mp3',
    '../stim/pitch/metta_semishort_129.mp3',
    '../stim/pitch/metta_semishort_148.mp3',
    '../stim/pitch/metta_semishort_191.mp3',
    '../stim/pitch/metta_semishort_66.mp3',
    '../stim/pitch/metta_short_110.mp3',
    '../stim/pitch/metta_short_129.mp3',
    '../stim/pitch/metta_short_148.mp3',
    '../stim/pitch/metta_short_191.mp3',
    '../stim/pitch/metta_short_66.mp3',
    '../stim/pitch/tate_long_110.mp3',
    '../stim/pitch/tate_long_127.mp3',
    '../stim/pitch/tate_long_144.mp3',
    '../stim/pitch/tate_long_183.mp3',
    '../stim/pitch/tate_long_72.mp3',
    '../stim/pitch/tate_semilong_110.mp3',
    '../stim/pitch/tate_semilong_127.mp3',
    '../stim/pitch/tate_semilong_144.mp3',
    '../stim/pitch/tate_semilong_183.mp3',
    '../stim/pitch/tate_semilong_72.mp3',
    '../stim/pitch/tate_semishort_110.mp3',
    '../stim/pitch/tate_semishort_127.mp3',
    '../stim/pitch/tate_semishort_144.mp3',
    '../stim/pitch/tate_semishort_183.mp3',
    '../stim/pitch/tate_semishort_72.mp3',
    '../stim/pitch/tate_short_110.mp3',
    '../stim/pitch/tate_short_127.mp3',
    '../stim/pitch/tate_short_144.mp3',
    '../stim/pitch/tate_short_183.mp3',
    '../stim/pitch/tate_short_72.mp3',
    '../stim/pitch/tatte_long_110.mp3',
    '../stim/pitch/tatte_long_127.mp3',
    '../stim/pitch/tatte_long_144.mp3',
    '../stim/pitch/tatte_long_183.mp3',
    '../stim/pitch/tatte_long_72.mp3',
    '../stim/pitch/tatte_semilong_110.mp3',
    '../stim/pitch/tatte_semilong_127.mp3',
    '../stim/pitch/tatte_semilong_144.mp3',
    '../stim/pitch/tatte_semilong_183.mp3',
    '../stim/pitch/tatte_semilong_72.mp3',
    '../stim/pitch/tatte_semishort_110.mp3',
    '../stim/pitch/tatte_semishort_127.mp3',
    '../stim/pitch/tatte_semishort_144.mp3',
    '../stim/pitch/tatte_semishort_183.mp3',
    '../stim/pitch/tatte_semishort_72.mp3',
    '../stim/pitch/tatte_short_110.mp3',
    '../stim/pitch/tatte_short_127.mp3',
    '../stim/pitch/tatte_short_144.mp3',
    '../stim/pitch/tatte_short_183.mp3',
    '../stim/pitch/tatte_short_72.mp3',
    '../stim/pitch/tosho_long_110.mp3',
    '../stim/pitch/tosho_long_127.mp3',
    '../stim/pitch/tosho_long_144.mp3',
    '../stim/pitch/tosho_long_183.mp3',
    '../stim/pitch/tosho_long_72.mp3',
    '../stim/pitch/tosho_semilong_110.mp3',
    '../stim/pitch/tosho_semilong_127.mp3',
    '../stim/pitch/tosho_semilong_144.mp3',
    '../stim/pitch/tosho_semilong_183.mp3',
    '../stim/pitch/tosho_semilong_72.mp3',
    '../stim/pitch/tosho_semishort_110.mp3',
    '../stim/pitch/tosho_semishort_127.mp3',
    '../stim/pitch/tosho_semishort_144.mp3',
    '../stim/pitch/tosho_semishort_183.mp3',
    '../stim/pitch/tosho_semishort_72.mp3',
    '../stim/pitch/tosho_short_110.mp3',
    '../stim/pitch/tosho_short_127.mp3',
    '../stim/pitch/tosho_short_144.mp3',
    '../stim/pitch/tosho_short_183.mp3',
    '../stim/pitch/tosho_short_72.mp3',
    '../stim/pitch/tousho_long_110.mp3',
    '../stim/pitch/tousho_long_127.mp3',
    '../stim/pitch/tousho_long_144.mp3',
    '../stim/pitch/tousho_long_183.mp3',
    '../stim/pitch/tousho_long_72.mp3',
    '../stim/pitch/tousho_semilong_110.mp3',
    '../stim/pitch/tousho_semilong_127.mp3',
    '../stim/pitch/tousho_semilong_144.mp3',
    '../stim/pitch/tousho_semilong_183.mp3',
    '../stim/pitch/tousho_semilong_72.mp3',
    '../stim/pitch/tousho_semishort_110.mp3',
    '../stim/pitch/tousho_semishort_127.mp3',
    '../stim/pitch/tousho_semishort_144.mp3',
    '../stim/pitch/tousho_semishort_183.mp3',
    '../stim/pitch/tousho_semishort_72.mp3',
    '../stim/pitch/tousho_short_110.mp3',
    '../stim/pitch/tousho_short_127.mp3',
    '../stim/pitch/tousho_short_144.mp3',
    '../stim/pitch/tousho_short_183.mp3',
    '../stim/pitch/tousho_short_72.mp3'
];

// save data function

function saveData(name, data){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data.php');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({filename: name, filedata: data}));
}

// run jsPsych

jsPsych.init({
    timeline: timeline,
    default_iti: 50,
    preload_audio: audiopaths,
    on_finish: function() {
        if (should_i_save === 1){
            var my_data = jsPsych.data.get().csv();
            var my_data_name = my_id + "_results";
            saveData(my_data_name, my_data);
        }
    }
});
