/* globals jsPsych */

var timeline = [];
var my_id = jsPsych.randomization.randomID(8);
var should_i_save = 1;
var completed_trials = 0;

// welcome page
var welcome = {
    type: 'html-keyboard-response',
    stimulus: `実験にようこそ。準備ができたら何でもキーを押してください。`
};

// Consent process

var agree = "私はこの承諾書の内容を理解し、" +
            "上記の実験に参加することを決断しました。";

var disagree = "私はこの承諾書の内容を理解し、" +
               "上記の実験に参加することを決断しませんでした。";


var consent_options = [agree, disagree];

var consent = {
    type: 'survey-multi-choice',
    preamble: `
<div style='text-align: left;'>
<h1>言語学実験</h1>
<h2>研究プロジェクト参加承諾</h2>

<h3>参加要請とプロジェクト概要</h3>

<p>
    この承諾書は言語学実験への参加を要請するものです。
    ご希望であれば、参加を断ることも可能です。
    あなたの言語知識が研究員にとって科学的関心に値すると判断されました。
    この研究調査に参加するか否かを判断していただく上で、調査の目的、リスク、
    そして利益を知っていただく必要があります。
    この承諾書は以下に、実験の詳細な進め方についての情報を記しています。
    この研究の代表者はトロント大学のカン・ユンジュン（Yoonjung Kang）教授です。
    この研究は、様々な言語環境に応じて、
    日本語話者による音声の知覚の仕方がどのように変化するかを
    調査することを目的としています。
    実験の進め方を理解していただいた段階で、参加するか否かを決定してください。
</p>

<h3> 実験内容 </h3>
<p>
    日本語母語話者によって発話された一連の文章を聴いていただき、
    聴き取った音声に最適である対象の単語を選択していただきます。
</p>

<h3>リスクや不都合</h3>
<p>
    この実験には、予想されうるリスクや不便はありません。
</p>

<h3>利益</h3> 
<p>
    この実験の調査内容から参加者に個人的な利益は発生しませんが、
    その他の人々にとって将来利益となりうる情報が研究員に託されます。
</p>

<h3>報酬</h3> 
<p>
    参加の報酬として、３３０円をお支払いします。
    実験に要する時間は１５〜２５分です。
</p>

<h3>個人情報の保護</h3> 
<p>
    参加者の名前は記録されることはありません。
    よって、後の学術的な発表や出版物に参加者の名前が記載されることはありません。
    この調査において集められたその他の個人情報はこの実験の研究員らや
    その共同研究者以外の誰にも開示されることはありません。 
    さらに、これらの情報は可能な限り安全な方法で管理され、
    調査結果が発表された後に削除する配慮がなされる見込みです。
    しかし、言語学研究の標準的な慣例に沿って、（参加者からの特別な依頼がないかぎり）
    収録された言語データは処分されません。
    なぜなら、より広範囲の言語話者のサンプルを含む更なる調査や、
    世代間における言語的変遷を調べる調査に、
    その保存されたデータが必要になる可能性があるためです。
    あなたの参加するこの研究調査は、
    関係する法律や規定が順守されているかを確認する優良性保証のための監査対象となります。
    もし選ばれた場合、Human Research Ethics Program （HREP）もしくは、
    人間を対象とする研究倫理プログラム の代表者に、
    監査の一環として、この調査に関係するデータや承諾書の内容を開示することがあります。
    HREPに開示されるすべての情報は、
    研究チームが上記すると同様の個人情報保護の配慮がなされます。
</p>

<h3>任意参加</h3> 
<p>
    あなたには、参加しないことを選択する権利があります。
    もし参加することを選択しても、どの段階においても、
    途中で実験を辞退することが可能です。
    参加を辞退した場合でも、将来実験に参加する権利を脅かすことはありません。
</p>

<h3>質問</h3> 
<p>
    この承諾書に署名する前に、参加するか否かを決断するのに必要な質問があれば、
    研究員にしてください。
    しかし、実験結果を左右しうる質問の答えは保留させていただきます。
    この実験に参加するか否かの判断に必要な時間は幾分にとっていただいて構いません。
</p>

<h3>問い合わせ先</h3>
<p>
    このプロジェクトに関してさらに質問がある場合は、
    研究代表カン・ユンジュン教授 （416-287-7172もしくはkang@utsc.utoronto.ca）
    に連絡してください。
    研究参加者としてのご自身の権利に関して質問がある場合は、
    the Office of Research Ethics（研究倫理オフィス、
    416-946-3273または ethics.review@utoronto.ca）に連絡してください。
</p>
      `,
    questions: [{
        prompt: "<h3>承諾表明</h3>",
        options: consent_options,
        required: true
    }],
    error_message: "この項目は必須です。",
    button_label: "次へ",
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        jsPsych.data.addProperties({ consent: responses.Q0 });
    }
};

var no_consent = {
    type: 'html-keyboard-response',
    stimulus: "お時間をいただきありがとうございました。",
    on_finish: function(){
        if (true){
            should_i_save = 0;
            jsPsych.endExperiment("お時間をいただきありがとうございます。");
        }
    }
};

var check_consent = {
    timeline: [no_consent],
    conditional_function: function(){
        var data = jsPsych.data.getLastTrialData().values()[0];
        var responses = JSON.parse(data.responses);
        if (responses.Q0 === disagree){
            return true;
        } else {
            return false;
        }
    }
};

// Ask the participant for demo info and save it to data
var demographic_questions = {
    type: 'survey-text',
    button_label: "次へ",
    error_message: "この項目は必須です。",
    questions: [
        {prompt: "生年", required: true},
        {prompt: "性別", required: true},
        {prompt: "出身の都道府県", required: true},
        {prompt: "話せる言語（日本語以外）", required: true}
    ],
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        var age = responses.Q0;
        var my_age = age;
        var gender = responses.Q1;
        var my_gender = gender;
        var origin = responses.Q2;
        var my_origin = origin;
        var languages = responses.Q3;
        var my_languages = languages;
        jsPsych.data.addProperties({
            subject_id: my_id,
            gender: my_gender,
            age: my_age,
            origin: my_origin,
            languages: my_languages}
        );
    }
};

// Instructions

var instructions = {
    type: 'html-keyboard-response',
    stimulus: "<p> この実験では日本語の話者が「竹内さんはとても穏やかに◯◯を発音した」" +
        "と言うのが聞こえます。 </p>" +
        "<p> ０か１のキーを押すと、竹内さんが発音した単語を選んで下さい。 </p>" +
        "<p> 準備ができたらスペースバーを押してください。 </p>",
    choices: [' ']
};


// Scales for test
var nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

var meta  = "１：メタ"   + nbsp + "０：滅多";
var tate  = "１：縦"     + nbsp + "０：立って";
var ika   = "１：以下"   + nbsp + "０：一家";
var kako  = "１：過去"   + nbsp + "０：括弧";

var kado  = "１：角"     + nbsp + "０：カード";
var biru  = "１：ビル"   + nbsp + "０：ビール";
var medo  = "１：目処"   + nbsp + "０：明度";
var tosho = "１：図書"   + nbsp + "０：当初";

// Test procedure
var trial = {
    type: 'audio-keyboard-response',
    choices: ['0', '1'],
    prompt: jsPsych.timelineVariable('opt'),
    stimulus: jsPsych.timelineVariable('audio'),
    on_finish: function(data){
        if (data.key_press === 48) {
            data.response = "long";
        } else {
            data.response = "short";
        }
        completed_trials += 1;
    }
};

var take_a_break = {
    type: 'html-keyboard-response',
    stimulus: `ちょっと休憩できます。
               続行する準備ができたら、
               スペースバーを押してください。`,
    choices: (' ')
};

var maybe_break = {
    timeline: [take_a_break],
    conditional_function: function(){
        if (completed_trials % 32 === 0) {
            return true;
        } else {
            return false;
        }
    }
};

var test_procedure = {
    timeline: [trial, maybe_break],
    timeline_variables: [
        { audio: '../stim/rate/biiru_fast_111.mp3', dur: 111, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_142.mp3', dur: 142, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_157.mp3', dur: 157, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_170.mp3', dur: 170, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_182.mp3', dur: 182, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_197.mp3', dur: 197, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_228.mp3', dur: 228, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_260.mp3', dur: 260, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_fast_80.mp3', dur: 80, opt: biru, orig: 'biiru', rate: 'fast' },
        { audio: '../stim/rate/biiru_slow_111.mp3', dur: 111, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_142.mp3', dur: 142, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_157.mp3', dur: 157, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_170.mp3', dur: 170, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_182.mp3', dur: 182, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_197.mp3', dur: 197, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_228.mp3', dur: 228, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_260.mp3', dur: 260, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biiru_slow_80.mp3', dur: 80, opt: biru, orig: 'biiru', rate: 'slow' },
        { audio: '../stim/rate/biru_fast_111.mp3', dur: 111, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_142.mp3', dur: 142, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_157.mp3', dur: 157, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_170.mp3', dur: 170, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_182.mp3', dur: 182, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_197.mp3', dur: 197, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_228.mp3', dur: 228, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_260.mp3', dur: 260, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_fast_80.mp3', dur: 80, opt: biru, orig: 'biru', rate: 'fast' },
        { audio: '../stim/rate/biru_slow_111.mp3', dur: 111, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_142.mp3', dur: 142, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_157.mp3', dur: 157, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_170.mp3', dur: 170, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_182.mp3', dur: 182, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_197.mp3', dur: 197, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_228.mp3', dur: 228, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_260.mp3', dur: 260, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/biru_slow_80.mp3', dur: 80, opt: biru, orig: 'biru', rate: 'slow' },
        { audio: '../stim/rate/ika_fast_110.mp3', dur: 110, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_120.mp3', dur: 120, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_127.mp3', dur: 127, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_135.mp3', dur: 135, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_145.mp3', dur: 145, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_165.mp3', dur: 165, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_184.mp3', dur: 184, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_70.mp3', dur: 70, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_fast_90.mp3', dur: 90, opt: ika, orig: 'ika', rate: 'fast' },
        { audio: '../stim/rate/ika_slow_110.mp3', dur: 110, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_120.mp3', dur: 120, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_127.mp3', dur: 127, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_135.mp3', dur: 135, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_145.mp3', dur: 145, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_165.mp3', dur: 165, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_184.mp3', dur: 184, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_70.mp3', dur: 70, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ika_slow_90.mp3', dur: 90, opt: ika, orig: 'ika', rate: 'slow' },
        { audio: '../stim/rate/ikka_fast_110.mp3', dur: 110, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_120.mp3', dur: 120, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_127.mp3', dur: 127, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_135.mp3', dur: 135, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_145.mp3', dur: 145, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_165.mp3', dur: 165, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_184.mp3', dur: 184, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_70.mp3', dur: 70, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_fast_90.mp3', dur: 90, opt: ika, orig: 'ikka', rate: 'fast' },
        { audio: '../stim/rate/ikka_slow_110.mp3', dur: 110, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_120.mp3', dur: 120, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_127.mp3', dur: 127, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_135.mp3', dur: 135, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_145.mp3', dur: 145, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_165.mp3', dur: 165, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_184.mp3', dur: 184, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_70.mp3', dur: 70, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/ikka_slow_90.mp3', dur: 90, opt: ika, orig: 'ikka', rate: 'slow' },
        { audio: '../stim/rate/kaado_fast_111.mp3', dur: 111, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_132.mp3', dur: 132, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_142.mp3', dur: 142, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_151.mp3', dur: 151, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_159.mp3', dur: 159, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_169.mp3', dur: 169, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_191.mp3', dur: 191, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_212.mp3', dur: 212, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_fast_89.mp3', dur: 89, opt: kado, orig: 'kaado', rate: 'fast' },
        { audio: '../stim/rate/kaado_slow_111.mp3', dur: 111, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_132.mp3', dur: 132, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_142.mp3', dur: 142, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_151.mp3', dur: 151, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_159.mp3', dur: 159, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_169.mp3', dur: 169, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_191.mp3', dur: 191, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_212.mp3', dur: 212, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kaado_slow_89.mp3', dur: 89, opt: kado, orig: 'kaado', rate: 'slow' },
        { audio: '../stim/rate/kado_fast_111.mp3', dur: 111, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_132.mp3', dur: 132, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_142.mp3', dur: 142, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_151.mp3', dur: 151, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_159.mp3', dur: 159, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_169.mp3', dur: 169, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_191.mp3', dur: 191, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_212.mp3', dur: 212, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_fast_89.mp3', dur: 89, opt: kado, orig: 'kado', rate: 'fast' },
        { audio: '../stim/rate/kado_slow_111.mp3', dur: 111, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_132.mp3', dur: 132, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_142.mp3', dur: 142, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_151.mp3', dur: 151, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_159.mp3', dur: 159, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_169.mp3', dur: 169, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_191.mp3', dur: 191, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_212.mp3', dur: 212, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kado_slow_89.mp3', dur: 89, opt: kado, orig: 'kado', rate: 'slow' },
        { audio: '../stim/rate/kakko_fast_109.mp3', dur: 109, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_117.mp3', dur: 117, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_124.mp3', dur: 124, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_131.mp3', dur: 131, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_140.mp3', dur: 140, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_157.mp3', dur: 157, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_174.mp3', dur: 174, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_74.mp3', dur: 74, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_fast_91.mp3', dur: 91, opt: kako, orig: 'kakko', rate: 'fast' },
        { audio: '../stim/rate/kakko_slow_109.mp3', dur: 109, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_117.mp3', dur: 117, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_124.mp3', dur: 124, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_131.mp3', dur: 131, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_140.mp3', dur: 140, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_157.mp3', dur: 157, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_174.mp3', dur: 174, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_74.mp3', dur: 74, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kakko_slow_91.mp3', dur: 91, opt: kako, orig: 'kakko', rate: 'slow' },
        { audio: '../stim/rate/kako_fast_109.mp3', dur: 109, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_117.mp3', dur: 117, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_124.mp3', dur: 124, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_131.mp3', dur: 131, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_140.mp3', dur: 140, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_157.mp3', dur: 157, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_174.mp3', dur: 174, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_74.mp3', dur: 74, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_fast_91.mp3', dur: 91, opt: kako, orig: 'kako', rate: 'fast' },
        { audio: '../stim/rate/kako_slow_109.mp3', dur: 109, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_117.mp3', dur: 117, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_124.mp3', dur: 124, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_131.mp3', dur: 131, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_140.mp3', dur: 140, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_157.mp3', dur: 157, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_174.mp3', dur: 174, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_74.mp3', dur: 74, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/kako_slow_91.mp3', dur: 91, opt: kako, orig: 'kako', rate: 'slow' },
        { audio: '../stim/rate/medo_fast_119.mp3', dur: 119, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_148.mp3', dur: 148, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_163.mp3', dur: 163, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_175.mp3', dur: 175, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_187.mp3', dur: 187, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_201.mp3', dur: 201, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_231.mp3', dur: 231, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_261.mp3', dur: 261, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_fast_89.mp3', dur: 89, opt: medo, orig: 'medo', rate: 'fast' },
        { audio: '../stim/rate/medo_slow_119.mp3', dur: 119, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_148.mp3', dur: 148, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_163.mp3', dur: 163, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_175.mp3', dur: 175, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_187.mp3', dur: 187, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_201.mp3', dur: 201, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_231.mp3', dur: 231, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_261.mp3', dur: 261, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/medo_slow_89.mp3', dur: 89, opt: medo, orig: 'medo', rate: 'slow' },
        { audio: '../stim/rate/meido_fast_119.mp3', dur: 119, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_148.mp3', dur: 148, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_163.mp3', dur: 163, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_175.mp3', dur: 175, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_187.mp3', dur: 187, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_201.mp3', dur: 201, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_231.mp3', dur: 231, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_261.mp3', dur: 261, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_fast_89.mp3', dur: 89, opt: medo, orig: 'meido', rate: 'fast' },
        { audio: '../stim/rate/meido_slow_119.mp3', dur: 119, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_148.mp3', dur: 148, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_163.mp3', dur: 163, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_175.mp3', dur: 175, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_187.mp3', dur: 187, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_201.mp3', dur: 201, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_231.mp3', dur: 231, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_261.mp3', dur: 261, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meido_slow_89.mp3', dur: 89, opt: medo, orig: 'meido', rate: 'slow' },
        { audio: '../stim/rate/meta_fast_110.mp3', dur: 110, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_120.mp3', dur: 120, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_129.mp3', dur: 129, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_138.mp3', dur: 138, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_148.mp3', dur: 148, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_170.mp3', dur: 170, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_191.mp3', dur: 191, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_66.mp3', dur: 66, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_fast_88.mp3', dur: 88, opt: meta, orig: 'meta', rate: 'fast' },
        { audio: '../stim/rate/meta_slow_110.mp3', dur: 110, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_120.mp3', dur: 120, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_129.mp3', dur: 129, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_138.mp3', dur: 138, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_148.mp3', dur: 148, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_170.mp3', dur: 170, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_191.mp3', dur: 191, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_66.mp3', dur: 66, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/meta_slow_88.mp3', dur: 88, opt: meta, orig: 'meta', rate: 'slow' },
        { audio: '../stim/rate/metta_fast_110.mp3', dur: 110, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_120.mp3', dur: 120, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_129.mp3', dur: 129, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_138.mp3', dur: 138, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_148.mp3', dur: 148, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_170.mp3', dur: 170, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_191.mp3', dur: 191, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_66.mp3', dur: 66, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_fast_88.mp3', dur: 88, opt: meta, orig: 'metta', rate: 'fast' },
        { audio: '../stim/rate/metta_slow_110.mp3', dur: 110, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_120.mp3', dur: 120, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_129.mp3', dur: 129, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_138.mp3', dur: 138, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_148.mp3', dur: 148, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_170.mp3', dur: 170, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_191.mp3', dur: 191, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_66.mp3', dur: 66, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/metta_slow_88.mp3', dur: 88, opt: meta, orig: 'metta', rate: 'slow' },
        { audio: '../stim/rate/tate_fast_110.mp3', dur: 110, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_120.mp3', dur: 120, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_127.mp3', dur: 127, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_135.mp3', dur: 135, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_144.mp3', dur: 144, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_163.mp3', dur: 163, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_183.mp3', dur: 183, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_72.mp3', dur: 72, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_fast_91.mp3', dur: 91, opt: tate, orig: 'tate', rate: 'fast' },
        { audio: '../stim/rate/tate_slow_110.mp3', dur: 110, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_120.mp3', dur: 120, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_127.mp3', dur: 127, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_135.mp3', dur: 135, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_144.mp3', dur: 144, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_163.mp3', dur: 163, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_183.mp3', dur: 183, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_72.mp3', dur: 72, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tate_slow_91.mp3', dur: 91, opt: tate, orig: 'tate', rate: 'slow' },
        { audio: '../stim/rate/tatte_fast_110.mp3', dur: 110, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_120.mp3', dur: 120, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_127.mp3', dur: 127, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_135.mp3', dur: 135, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_144.mp3', dur: 144, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_163.mp3', dur: 163, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_183.mp3', dur: 183, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_72.mp3', dur: 72, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_fast_91.mp3', dur: 91, opt: tate, orig: 'tatte', rate: 'fast' },
        { audio: '../stim/rate/tatte_slow_110.mp3', dur: 110, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_120.mp3', dur: 120, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_127.mp3', dur: 127, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_135.mp3', dur: 135, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_144.mp3', dur: 144, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_163.mp3', dur: 163, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_183.mp3', dur: 183, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_72.mp3', dur: 72, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tatte_slow_91.mp3', dur: 91, opt: tate, orig: 'tatte', rate: 'slow' },
        { audio: '../stim/rate/tosho_fast_110.mp3', dur: 110, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_120.mp3', dur: 120, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_127.mp3', dur: 127, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_135.mp3', dur: 135, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_144.mp3', dur: 144, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_163.mp3', dur: 163, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_183.mp3', dur: 183, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_72.mp3', dur: 72, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_fast_91.mp3', dur: 91, opt: tosho, orig: 'tosho', rate: 'fast' },
        { audio: '../stim/rate/tosho_slow_110.mp3', dur: 110, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_120.mp3', dur: 120, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_127.mp3', dur: 127, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_135.mp3', dur: 135, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_144.mp3', dur: 144, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_163.mp3', dur: 163, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_183.mp3', dur: 183, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_72.mp3', dur: 72, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tosho_slow_91.mp3', dur: 91, opt: tosho, orig: 'tosho', rate: 'slow' },
        { audio: '../stim/rate/tousho_fast_110.mp3', dur: 110, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_120.mp3', dur: 120, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_127.mp3', dur: 127, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_135.mp3', dur: 135, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_144.mp3', dur: 144, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_163.mp3', dur: 163, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_183.mp3', dur: 183, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_72.mp3', dur: 72, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_fast_91.mp3', dur: 91, opt: tosho, orig: 'tousho', rate: 'fast' },
        { audio: '../stim/rate/tousho_slow_110.mp3', dur: 110, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_120.mp3', dur: 120, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_127.mp3', dur: 127, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_135.mp3', dur: 135, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_144.mp3', dur: 144, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_163.mp3', dur: 163, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_183.mp3', dur: 183, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_72.mp3', dur: 72, opt: tosho, orig: 'tousho', rate: 'slow' },
        { audio: '../stim/rate/tousho_slow_91.mp3', dur: 91, opt: tosho, orig: 'tousho', rate: 'slow' }
    ],
    randomize_order: true,
    data: {
        file: jsPsych.timelineVariable('audio'),
        duration: jsPsych.timelineVariable('dur'),
        original_word: jsPsych.timelineVariable('orig'),
        rate: jsPsych.timelineVariable('rate')
    }
};

// ending page

var ending = {
    type: 'html-keyboard-response',
    stimulus: "<p>実験に参加して下さってありがとうございます。</p>" +
    "<p>データを保存するにはスペースバーを押して下さい。</p>" +
    "<p>データが保存されたら、認証コードが表示されます。",
    response_ends_trial: true,
    choices: [' '],
    on_finish: function(){
        jsPsych.endExperiment(
            "<p>データが保存されました。</p>" +
            "<p> 認証コードは「" +
            my_id +
            "」です。そのコードをクラウドワークスに入力してください。</p>" +
            "<p>コードを忘れないように書き留めてください。</p>" +
            "<p>コードを書き留めたら、このウィンドーを閉じることができます。"
        );
    }
};

// timeline

var timeline = [];
timeline.push(welcome);
timeline.push(consent);
timeline.push(check_consent);
timeline.push(demographic_questions);
timeline.push(instructions);
timeline.push(test_procedure);
timeline.push(ending);

// preload audio

var audiopaths = [
    '../stim/rate/biiru_fast_111.mp3', '../stim/rate/biiru_fast_142.mp3',
    '../stim/rate/biiru_fast_157.mp3', '../stim/rate/biiru_fast_170.mp3',
    '../stim/rate/biiru_fast_182.mp3', '../stim/rate/biiru_fast_197.mp3',
    '../stim/rate/biiru_fast_228.mp3', '../stim/rate/biiru_fast_260.mp3',
    '../stim/rate/biiru_fast_80.mp3', '../stim/rate/biiru_slow_111.mp3',
    '../stim/rate/biiru_slow_142.mp3', '../stim/rate/biiru_slow_157.mp3',
    '../stim/rate/biiru_slow_170.mp3', '../stim/rate/biiru_slow_182.mp3',
    '../stim/rate/biiru_slow_197.mp3', '../stim/rate/biiru_slow_228.mp3',
    '../stim/rate/biiru_slow_260.mp3', '../stim/rate/biiru_slow_80.mp3',
    '../stim/rate/biru_fast_111.mp3', '../stim/rate/biru_fast_142.mp3',
    '../stim/rate/biru_fast_157.mp3', '../stim/rate/biru_fast_170.mp3',
    '../stim/rate/biru_fast_182.mp3', '../stim/rate/biru_fast_197.mp3',
    '../stim/rate/biru_fast_228.mp3', '../stim/rate/biru_fast_260.mp3',
    '../stim/rate/biru_fast_80.mp3', '../stim/rate/biru_slow_111.mp3',
    '../stim/rate/biru_slow_142.mp3', '../stim/rate/biru_slow_157.mp3',
    '../stim/rate/biru_slow_170.mp3', '../stim/rate/biru_slow_182.mp3',
    '../stim/rate/biru_slow_197.mp3', '../stim/rate/biru_slow_228.mp3',
    '../stim/rate/biru_slow_260.mp3', '../stim/rate/biru_slow_80.mp3',
    '../stim/rate/ika_fast_110.mp3', '../stim/rate/ika_fast_120.mp3',
    '../stim/rate/ika_fast_127.mp3', '../stim/rate/ika_fast_135.mp3',
    '../stim/rate/ika_fast_145.mp3', '../stim/rate/ika_fast_165.mp3',
    '../stim/rate/ika_fast_184.mp3', '../stim/rate/ika_fast_70.mp3',
    '../stim/rate/ika_fast_90.mp3', '../stim/rate/ika_slow_110.mp3',
    '../stim/rate/ika_slow_120.mp3', '../stim/rate/ika_slow_127.mp3',
    '../stim/rate/ika_slow_135.mp3', '../stim/rate/ika_slow_145.mp3',
    '../stim/rate/ika_slow_165.mp3', '../stim/rate/ika_slow_184.mp3',
    '../stim/rate/ika_slow_70.mp3', '../stim/rate/ika_slow_90.mp3',
    '../stim/rate/ikka_fast_110.mp3', '../stim/rate/ikka_fast_120.mp3',
    '../stim/rate/ikka_fast_127.mp3', '../stim/rate/ikka_fast_135.mp3',
    '../stim/rate/ikka_fast_145.mp3', '../stim/rate/ikka_fast_165.mp3',
    '../stim/rate/ikka_fast_184.mp3', '../stim/rate/ikka_fast_70.mp3',
    '../stim/rate/ikka_fast_90.mp3', '../stim/rate/ikka_slow_110.mp3',
    '../stim/rate/ikka_slow_120.mp3', '../stim/rate/ikka_slow_127.mp3',
    '../stim/rate/ikka_slow_135.mp3', '../stim/rate/ikka_slow_145.mp3',
    '../stim/rate/ikka_slow_165.mp3', '../stim/rate/ikka_slow_184.mp3',
    '../stim/rate/ikka_slow_70.mp3', '../stim/rate/ikka_slow_90.mp3',
    '../stim/rate/kaado_fast_111.mp3', '../stim/rate/kaado_fast_132.mp3',
    '../stim/rate/kaado_fast_142.mp3', '../stim/rate/kaado_fast_151.mp3',
    '../stim/rate/kaado_fast_159.mp3', '../stim/rate/kaado_fast_169.mp3',
    '../stim/rate/kaado_fast_191.mp3', '../stim/rate/kaado_fast_212.mp3',
    '../stim/rate/kaado_fast_89.mp3', '../stim/rate/kaado_slow_111.mp3',
    '../stim/rate/kaado_slow_132.mp3', '../stim/rate/kaado_slow_142.mp3',
    '../stim/rate/kaado_slow_151.mp3', '../stim/rate/kaado_slow_159.mp3',
    '../stim/rate/kaado_slow_169.mp3', '../stim/rate/kaado_slow_191.mp3',
    '../stim/rate/kaado_slow_212.mp3', '../stim/rate/kaado_slow_89.mp3',
    '../stim/rate/kado_fast_111.mp3', '../stim/rate/kado_fast_132.mp3',
    '../stim/rate/kado_fast_142.mp3', '../stim/rate/kado_fast_151.mp3',
    '../stim/rate/kado_fast_159.mp3', '../stim/rate/kado_fast_169.mp3',
    '../stim/rate/kado_fast_191.mp3', '../stim/rate/kado_fast_212.mp3',
    '../stim/rate/kado_fast_89.mp3', '../stim/rate/kado_slow_111.mp3',
    '../stim/rate/kado_slow_132.mp3', '../stim/rate/kado_slow_142.mp3',
    '../stim/rate/kado_slow_151.mp3', '../stim/rate/kado_slow_159.mp3',
    '../stim/rate/kado_slow_169.mp3', '../stim/rate/kado_slow_191.mp3',
    '../stim/rate/kado_slow_212.mp3', '../stim/rate/kado_slow_89.mp3',
    '../stim/rate/kakko_fast_109.mp3', '../stim/rate/kakko_fast_117.mp3',
    '../stim/rate/kakko_fast_124.mp3', '../stim/rate/kakko_fast_131.mp3',
    '../stim/rate/kakko_fast_140.mp3', '../stim/rate/kakko_fast_157.mp3',
    '../stim/rate/kakko_fast_174.mp3', '../stim/rate/kakko_fast_74.mp3',
    '../stim/rate/kakko_fast_91.mp3', '../stim/rate/kakko_slow_109.mp3',
    '../stim/rate/kakko_slow_117.mp3', '../stim/rate/kakko_slow_124.mp3',
    '../stim/rate/kakko_slow_131.mp3', '../stim/rate/kakko_slow_140.mp3',
    '../stim/rate/kakko_slow_157.mp3', '../stim/rate/kakko_slow_174.mp3',
    '../stim/rate/kakko_slow_74.mp3', '../stim/rate/kakko_slow_91.mp3',
    '../stim/rate/kako_fast_109.mp3', '../stim/rate/kako_fast_117.mp3',
    '../stim/rate/kako_fast_124.mp3', '../stim/rate/kako_fast_131.mp3',
    '../stim/rate/kako_fast_140.mp3', '../stim/rate/kako_fast_157.mp3',
    '../stim/rate/kako_fast_174.mp3', '../stim/rate/kako_fast_74.mp3',
    '../stim/rate/kako_fast_91.mp3', '../stim/rate/kako_slow_109.mp3',
    '../stim/rate/kako_slow_117.mp3', '../stim/rate/kako_slow_124.mp3',
    '../stim/rate/kako_slow_131.mp3', '../stim/rate/kako_slow_140.mp3',
    '../stim/rate/kako_slow_157.mp3', '../stim/rate/kako_slow_174.mp3',
    '../stim/rate/kako_slow_74.mp3', '../stim/rate/kako_slow_91.mp3',
    '../stim/rate/medo_fast_119.mp3', '../stim/rate/medo_fast_148.mp3',
    '../stim/rate/medo_fast_163.mp3', '../stim/rate/medo_fast_175.mp3',
    '../stim/rate/medo_fast_187.mp3', '../stim/rate/medo_fast_201.mp3',
    '../stim/rate/medo_fast_231.mp3', '../stim/rate/medo_fast_261.mp3',
    '../stim/rate/medo_fast_89.mp3', '../stim/rate/medo_slow_119.mp3',
    '../stim/rate/medo_slow_148.mp3', '../stim/rate/medo_slow_163.mp3',
    '../stim/rate/medo_slow_175.mp3', '../stim/rate/medo_slow_187.mp3',
    '../stim/rate/medo_slow_201.mp3', '../stim/rate/medo_slow_231.mp3',
    '../stim/rate/medo_slow_261.mp3', '../stim/rate/medo_slow_89.mp3',
    '../stim/rate/meido_fast_119.mp3', '../stim/rate/meido_fast_148.mp3',
    '../stim/rate/meido_fast_163.mp3', '../stim/rate/meido_fast_175.mp3',
    '../stim/rate/meido_fast_187.mp3', '../stim/rate/meido_fast_201.mp3',
    '../stim/rate/meido_fast_231.mp3', '../stim/rate/meido_fast_261.mp3',
    '../stim/rate/meido_fast_89.mp3', '../stim/rate/meido_slow_119.mp3',
    '../stim/rate/meido_slow_148.mp3', '../stim/rate/meido_slow_163.mp3',
    '../stim/rate/meido_slow_175.mp3', '../stim/rate/meido_slow_187.mp3',
    '../stim/rate/meido_slow_201.mp3', '../stim/rate/meido_slow_231.mp3',
    '../stim/rate/meido_slow_261.mp3', '../stim/rate/meido_slow_89.mp3',
    '../stim/rate/meta_fast_110.mp3', '../stim/rate/meta_fast_120.mp3',
    '../stim/rate/meta_fast_129.mp3', '../stim/rate/meta_fast_138.mp3',
    '../stim/rate/meta_fast_148.mp3', '../stim/rate/meta_fast_170.mp3',
    '../stim/rate/meta_fast_191.mp3', '../stim/rate/meta_fast_66.mp3',
    '../stim/rate/meta_fast_88.mp3', '../stim/rate/meta_slow_110.mp3',
    '../stim/rate/meta_slow_120.mp3', '../stim/rate/meta_slow_129.mp3',
    '../stim/rate/meta_slow_138.mp3', '../stim/rate/meta_slow_148.mp3',
    '../stim/rate/meta_slow_170.mp3', '../stim/rate/meta_slow_191.mp3',
    '../stim/rate/meta_slow_66.mp3', '../stim/rate/meta_slow_88.mp3',
    '../stim/rate/metta_fast_110.mp3', '../stim/rate/metta_fast_120.mp3',
    '../stim/rate/metta_fast_129.mp3', '../stim/rate/metta_fast_138.mp3',
    '../stim/rate/metta_fast_148.mp3', '../stim/rate/metta_fast_170.mp3',
    '../stim/rate/metta_fast_191.mp3', '../stim/rate/metta_fast_66.mp3',
    '../stim/rate/metta_fast_88.mp3', '../stim/rate/metta_slow_110.mp3',
    '../stim/rate/metta_slow_120.mp3', '../stim/rate/metta_slow_129.mp3',
    '../stim/rate/metta_slow_138.mp3', '../stim/rate/metta_slow_148.mp3',
    '../stim/rate/metta_slow_170.mp3', '../stim/rate/metta_slow_191.mp3',
    '../stim/rate/metta_slow_66.mp3', '../stim/rate/metta_slow_88.mp3',
    '../stim/rate/tate_fast_110.mp3', '../stim/rate/tate_fast_120.mp3',
    '../stim/rate/tate_fast_127.mp3', '../stim/rate/tate_fast_135.mp3',
    '../stim/rate/tate_fast_144.mp3', '../stim/rate/tate_fast_163.mp3',
    '../stim/rate/tate_fast_183.mp3', '../stim/rate/tate_fast_72.mp3',
    '../stim/rate/tate_fast_91.mp3', '../stim/rate/tate_slow_110.mp3',
    '../stim/rate/tate_slow_120.mp3', '../stim/rate/tate_slow_127.mp3',
    '../stim/rate/tate_slow_135.mp3', '../stim/rate/tate_slow_144.mp3',
    '../stim/rate/tate_slow_163.mp3', '../stim/rate/tate_slow_183.mp3',
    '../stim/rate/tate_slow_72.mp3', '../stim/rate/tate_slow_91.mp3',
    '../stim/rate/tatte_fast_110.mp3', '../stim/rate/tatte_fast_120.mp3',
    '../stim/rate/tatte_fast_127.mp3', '../stim/rate/tatte_fast_135.mp3',
    '../stim/rate/tatte_fast_144.mp3', '../stim/rate/tatte_fast_163.mp3',
    '../stim/rate/tatte_fast_183.mp3', '../stim/rate/tatte_fast_72.mp3',
    '../stim/rate/tatte_fast_91.mp3', '../stim/rate/tatte_slow_110.mp3',
    '../stim/rate/tatte_slow_120.mp3', '../stim/rate/tatte_slow_127.mp3',
    '../stim/rate/tatte_slow_135.mp3', '../stim/rate/tatte_slow_144.mp3',
    '../stim/rate/tatte_slow_163.mp3', '../stim/rate/tatte_slow_183.mp3',
    '../stim/rate/tatte_slow_72.mp3', '../stim/rate/tatte_slow_91.mp3',
    '../stim/rate/tosho_fast_110.mp3', '../stim/rate/tosho_fast_120.mp3',
    '../stim/rate/tosho_fast_127.mp3', '../stim/rate/tosho_fast_135.mp3',
    '../stim/rate/tosho_fast_144.mp3', '../stim/rate/tosho_fast_163.mp3',
    '../stim/rate/tosho_fast_183.mp3', '../stim/rate/tosho_fast_72.mp3',
    '../stim/rate/tosho_fast_91.mp3', '../stim/rate/tosho_slow_110.mp3',
    '../stim/rate/tosho_slow_120.mp3', '../stim/rate/tosho_slow_127.mp3',
    '../stim/rate/tosho_slow_135.mp3', '../stim/rate/tosho_slow_144.mp3',
    '../stim/rate/tosho_slow_163.mp3', '../stim/rate/tosho_slow_183.mp3',
    '../stim/rate/tosho_slow_72.mp3', '../stim/rate/tosho_slow_91.mp3',
    '../stim/rate/tousho_fast_110.mp3', '../stim/rate/tousho_fast_120.mp3',
    '../stim/rate/tousho_fast_127.mp3', '../stim/rate/tousho_fast_135.mp3',
    '../stim/rate/tousho_fast_144.mp3', '../stim/rate/tousho_fast_163.mp3',
    '../stim/rate/tousho_fast_183.mp3', '../stim/rate/tousho_fast_72.mp3',
    '../stim/rate/tousho_fast_91.mp3', '../stim/rate/tousho_slow_110.mp3',
    '../stim/rate/tousho_slow_120.mp3', '../stim/rate/tousho_slow_127.mp3',
    '../stim/rate/tousho_slow_135.mp3', '../stim/rate/tousho_slow_144.mp3',
    '../stim/rate/tousho_slow_163.mp3', '../stim/rate/tousho_slow_183.mp3',
    '../stim/rate/tousho_slow_72.mp3', '../stim/rate/tousho_slow_91.mp3'
];

// save data function

function saveData(name, data){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data_2.php');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({filename: name, filedata: data}));
}

// run jsPsych

jsPsych.init({
    timeline: timeline,
    default_iti: 50,
    preload_audio: audiopaths,
    on_finish: function() {
        if (should_i_save === 1){
            var my_data = jsPsych.data.get().csv();
            var my_data_name = my_id + "_results";
            saveData(my_data_name, my_data);
        }
    }
});
