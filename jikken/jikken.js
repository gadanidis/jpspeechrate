/* globals jsPsych */

var timeline = [];
var my_id = jsPsych.randomization.randomID(8);
var should_i_save = 1;
var completed_trials = 0;

// welcome page
var welcome = {
    type: 'html-keyboard-response',
    stimulus: `実験にようこそ。準備ができたら何でもキーを押してください。`
};

// Consent process

var agree = "私はこの承諾書の内容を理解し、" +
            "上記の実験に参加することを決断しました。";

var disagree = "私はこの承諾書の内容を理解し、" +
               "上記の実験に参加することを決断しませんでした。";


var consent_options = [agree, disagree];

var consent = {
    type: 'survey-multi-choice',
    preamble: `
<div style='text-align: left;'>
<h1>言語学実験</h1>
<h2>研究プロジェクト参加承諾</h2>

<h3>参加要請とプロジェクト概要</h3>

<p>
    この承諾書は言語学実験への参加を要請するものです。
    ご希望であれば、参加を断ることも可能です。
    あなたの言語知識が研究員にとって科学的関心に値すると判断されました。
    この研究調査に参加するか否かを判断していただく上で、調査の目的、リスク、
    そして利益を知っていただく必要があります。
    この承諾書は以下に、実験の詳細な進め方についての情報を記しています。
    この研究の代表者はトロント大学のカン・ユンジュン（Yoonjung Kang）教授です。
    この研究は、様々な言語環境に応じて、
    日本語話者による音声の知覚の仕方がどのように変化するかを
    調査することを目的としています。
    実験の進め方を理解していただいた段階で、参加するか否かを決定してください。
</p>

<h3> 実験内容 </h3>
<p>
    日本語母語話者によって発話された一連の文章を聴いていただき、
    聴き取った音声に最適である対象の単語を選択していただきます。
</p>

<h3>リスクや不都合</h3>
<p>
    この実験には、予想されうるリスクや不便はありません。
</p>

<h3>利益</h3> 
<p>
    この実験の調査内容から参加者に個人的な利益は発生しませんが、
    その他の人々にとって将来利益となりうる情報が研究員に託されます。
</p>

<h3>報酬</h3> 
<p>
    参加の報酬として、３３０円をお支払いします。
    実験に要する時間は１５〜２５分です。
</p>

<h3>個人情報の保護</h3> 
<p>
    参加者の名前は記録されることはありません。
    よって、後の学術的な発表や出版物に参加者の名前が記載されることはありません。
    この調査において集められたその他の個人情報はこの実験の研究員らや
    その共同研究者以外の誰にも開示されることはありません。 
    さらに、これらの情報は可能な限り安全な方法で管理され、
    調査結果が発表された後に削除する配慮がなされる見込みです。
    しかし、言語学研究の標準的な慣例に沿って、（参加者からの特別な依頼がないかぎり）
    収録された言語データは処分されません。
    なぜなら、より広範囲の言語話者のサンプルを含む更なる調査や、
    世代間における言語的変遷を調べる調査に、
    その保存されたデータが必要になる可能性があるためです。
    あなたの参加するこの研究調査は、
    関係する法律や規定が順守されているかを確認する優良性保証のための監査対象となります。
    もし選ばれた場合、Human Research Ethics Program （HREP）もしくは、
    人間を対象とする研究倫理プログラム の代表者に、
    監査の一環として、この調査に関係するデータや承諾書の内容を開示することがあります。
    HREPに開示されるすべての情報は、
    研究チームが上記すると同様の個人情報保護の配慮がなされます。
</p>

<h3>任意参加</h3> 
<p>
    あなたには、参加しないことを選択する権利があります。
    もし参加することを選択しても、どの段階においても、
    途中で実験を辞退することが可能です。
    参加を辞退した場合でも、将来実験に参加する権利を脅かすことはありません。
</p>

<h3>質問</h3> 
<p>
    この承諾書に署名する前に、参加するか否かを決断するのに必要な質問があれば、
    研究員にしてください。
    しかし、実験結果を左右しうる質問の答えは保留させていただきます。
    この実験に参加するか否かの判断に必要な時間は幾分にとっていただいて構いません。
</p>

<h3>問い合わせ先</h3>
<p>
    このプロジェクトに関してさらに質問がある場合は、
    研究代表カン・ユンジュン教授 （416-287-7172もしくはkang@utsc.utoronto.ca）
    に連絡してください。
    研究参加者としてのご自身の権利に関して質問がある場合は、
    the Office of Research Ethics（研究倫理オフィス、
    416-946-3273または ethics.review@utoronto.ca）に連絡してください。
</p>
      `,
    questions: [{
        prompt: "<h3>承諾表明</h3>",
        options: consent_options,
        required: true
    }],
    button_label: "次へ",
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        jsPsych.data.addProperties({ consent: responses.Q0 });
    }
};

var no_consent = {
    type: 'html-keyboard-response',
    stimulus: "お時間をいただきありがとうございました。",
    on_finish: function(){
        if (true){
            should_i_save = 0;
            jsPsych.endExperiment("お時間をいただきありがとうございます。");
        }
    }
};

var check_consent = {
    timeline: [no_consent],
    conditional_function: function(){
        var data = jsPsych.data.getLastTrialData().values()[0];
        var responses = JSON.parse(data.responses);
        if (responses.Q0 === disagree){
            return true;
        } else {
            return false;
        }
    }
};

// Ask the participant for demo info and save it to data
var demographic_questions = {
    type: 'survey-text',
    questions: [
        {prompt: "何歳ですか。", required: true},
        {prompt: "性別は何ですか。", required: true},
        {prompt: "出身はどちらですか。", required: true},
        {prompt: "日本語以外何語をは話せますか。", required: true}
    ],
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        var age = responses.Q0;
        var my_age = age;
        var gender = responses.Q1;
        var my_gender = gender;
        var origin = responses.Q2;
        var my_origin = origin;
        jsPsych.data.addProperties({
            subject_id: my_id,
            gender: my_gender,
            age: my_age,
            origin: my_origin}
        );
    }
};

// Instructions

var instructions = {
    type: 'html-keyboard-response',
    stimulus: "<p> この実験では日本語の話者が「竹内さんはとても穏やかに◯◯を発音した」" +
        "と言うのが聞こえます。 </p>" +
        "<p> ０か１のキーを押すと、竹内さんが発音した単語を選んで下さい。 </p>" +
        "<p> 準備ができたらスペースバーを押してください。 </p>",
    choices: [' ']
};


// Scales for test
var nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

var meta  = "１：メタ"   + nbsp + "０：滅多";
var tate  = "１：縦"     + nbsp + "０：立って";
var ika   = "１：以下"   + nbsp + "０：一家";
var kako  = "１：過去"   + nbsp + "０：括弧";

var kado  = "１：角"     + nbsp + "０：カード";
var biru  = "１：ビル"   + nbsp + "０：ビール";
var juri  = "１：ゆり"   + nbsp + "０：有利";
var medo  = "１：目処"   + nbsp + "０：明度";
var tosho = "１：図書"   + nbsp + "０：当初";

// Test procedure
var trial = {
    type: 'audio-keyboard-response',
    choices: ['0', '1'],
    prompt: jsPsych.timelineVariable('opt'),
    stimulus: jsPsych.timelineVariable('audio'),
    on_finish: function(data){
        if (data.key_press === 48) {
            data.response = "long";
        } else {
            data.response = "short";
        }
        completed_trials += 1
    }
};

var take_a_break = {
    type: 'html-keyboard-response',
    stimulus: `ちょっと休憩できます。
               続行する準備ができたら、
               スペースバーを押してください。`,
    choices: (' ')
};

var maybe_break = {
    timeline: [take_a_break],
    conditional_function: function(){
        if (completed_trials % 30 === 0) {
            return true;
        } else {
            return false;
        }
    }
}


var test_procedure = {
    timeline: [trial, maybe_break],
    timeline_variables: [
        { audio: '../stim/no_rate/biiru_100.mp3', dur: 100, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_114.mp3', dur: 114, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_129.mp3', dur: 129, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_143.mp3', dur: 143, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_158.mp3', dur: 158, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_172.mp3', dur: 172, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_187.mp3', dur: 187, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_201.mp3', dur: 201, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_216.mp3', dur: 216, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biiru_230.mp3', dur: 230, opt: biru, orig: 'biiru' },
        { audio: '../stim/no_rate/biru_100.mp3', dur: 100, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_114.mp3', dur: 114, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_129.mp3', dur: 129, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_143.mp3', dur: 143, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_158.mp3', dur: 158, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_172.mp3', dur: 172, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_187.mp3', dur: 187, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_201.mp3', dur: 201, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_216.mp3', dur: 216, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/biru_230.mp3', dur: 230, opt: biru, orig: 'biru' },
        { audio: '../stim/no_rate/ika_113.mp3', dur: 113, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_130.mp3', dur: 130, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_147.mp3', dur: 147, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_163.mp3', dur: 163, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_180.mp3', dur: 180, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_197.mp3', dur: 197, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_213.mp3', dur: 213, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_230.mp3', dur: 230, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_80.mp3', dur: 80, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ika_97.mp3', dur: 97, opt: ika, orig: 'ika' },
        { audio: '../stim/no_rate/ikka_113.mp3', dur: 113, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_130.mp3', dur: 130, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_147.mp3', dur: 147, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_163.mp3', dur: 163, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_180.mp3', dur: 180, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_197.mp3', dur: 197, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_213.mp3', dur: 213, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_230.mp3', dur: 230, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_80.mp3', dur: 80, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/ikka_97.mp3', dur: 97, opt: ika, orig: 'ikka' },
        { audio: '../stim/no_rate/juri_104.mp3', dur: 104, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_117.mp3', dur: 117, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_129.mp3', dur: 129, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_141.mp3', dur: 141, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_153.mp3', dur: 153, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_166.mp3', dur: 166, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_178.mp3', dur: 178, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_190.mp3', dur: 190, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_80.mp3', dur: 80, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juri_92.mp3', dur: 92, opt: juri, orig: 'juri' },
        { audio: '../stim/no_rate/juuri_104.mp3', dur: 104, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_117.mp3', dur: 117, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_129.mp3', dur: 129, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_141.mp3', dur: 141, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_153.mp3', dur: 153, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_166.mp3', dur: 166, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_178.mp3', dur: 178, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_190.mp3', dur: 190, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_80.mp3', dur: 80, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/juuri_92.mp3', dur: 92, opt: juri, orig: 'juuri' },
        { audio: '../stim/no_rate/kaado_106.mp3', dur: 106, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_121.mp3', dur: 121, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_137.mp3', dur: 137, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_152.mp3', dur: 152, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_168.mp3', dur: 168, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_183.mp3', dur: 183, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_199.mp3', dur: 199, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_214.mp3', dur: 214, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_230.mp3', dur: 230, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kaado_90.mp3', dur: 90, opt: kado, orig: 'kaao' },
        { audio: '../stim/no_rate/kado_106.mp3', dur: 106, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_121.mp3', dur: 121, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_137.mp3', dur: 137, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_152.mp3', dur: 152, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_168.mp3', dur: 168, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_183.mp3', dur: 183, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_199.mp3', dur: 199, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_214.mp3', dur: 214, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_230.mp3', dur: 230, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kado_90.mp3', dur: 90, opt: kado, orig: 'kao' },
        { audio: '../stim/no_rate/kakko_103.mp3', dur: 103, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_120.mp3', dur: 120, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_137.mp3', dur: 137, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_153.mp3', dur: 153, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_170.mp3', dur: 170, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_187.mp3', dur: 187, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_203.mp3', dur: 203, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_220.mp3', dur: 220, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_70.mp3', dur: 70, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kakko_87.mp3', dur: 87, opt: kako, orig: 'kakko' },
        { audio: '../stim/no_rate/kako_103.mp3', dur: 103, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_120.mp3', dur: 120, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_137.mp3', dur: 137, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_153.mp3', dur: 153, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_170.mp3', dur: 170, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_187.mp3', dur: 187, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_203.mp3', dur: 203, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_220.mp3', dur: 220, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_70.mp3', dur: 70, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/kako_87.mp3', dur: 87, opt: kako, orig: 'kako' },
        { audio: '../stim/no_rate/medo_110.mp3', dur: 110, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_123.mp3', dur: 123, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_137.mp3', dur: 137, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_150.mp3', dur: 150, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_163.mp3', dur: 163, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_177.mp3', dur: 177, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_190.mp3', dur: 190, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_203.mp3', dur: 203, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_217.mp3', dur: 217, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/medo_230.mp3', dur: 230, opt: medo, orig: 'medo' },
        { audio: '../stim/no_rate/meido_110.mp3', dur: 110, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_123.mp3', dur: 123, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_137.mp3', dur: 137, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_150.mp3', dur: 150, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_163.mp3', dur: 163, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_177.mp3', dur: 177, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_190.mp3', dur: 190, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_203.mp3', dur: 203, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_217.mp3', dur: 217, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meido_230.mp3', dur: 230, opt: medo, orig: 'meido' },
        { audio: '../stim/no_rate/meta_113.mp3', dur: 113, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_130.mp3', dur: 130, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_147.mp3', dur: 147, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_163.mp3', dur: 163, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_180.mp3', dur: 180, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_197.mp3', dur: 197, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_213.mp3', dur: 213, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_230.mp3', dur: 230, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_80.mp3', dur: 80, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/meta_97.mp3', dur: 97, opt: meta, orig: 'meta' },
        { audio: '../stim/no_rate/metta_113.mp3', dur: 113, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_130.mp3', dur: 130, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_147.mp3', dur: 147, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_163.mp3', dur: 163, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_180.mp3', dur: 180, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_197.mp3', dur: 197, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_213.mp3', dur: 213, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_230.mp3', dur: 230, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_80.mp3', dur: 80, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/metta_97.mp3', dur: 97, opt: meta, orig: 'metta' },
        { audio: '../stim/no_rate/tate_110.mp3', dur: 110, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_133.mp3', dur: 133, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_157.mp3', dur: 157, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_180.mp3', dur: 180, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_203.mp3', dur: 203, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_227.mp3', dur: 227, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_250.mp3', dur: 250, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_40.mp3', dur: 40, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_63.mp3', dur: 63, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tate_87.mp3', dur: 87, opt: tate, orig: 'tate' },
        { audio: '../stim/no_rate/tatte_110.mp3', dur: 110, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_133.mp3', dur: 133, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_157.mp3', dur: 157, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_180.mp3', dur: 180, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_203.mp3', dur: 203, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_227.mp3', dur: 227, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_250.mp3', dur: 250, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_40.mp3', dur: 40, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_63.mp3', dur: 63, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tatte_87.mp3', dur: 87, opt: tate, orig: 'tatte' },
        { audio: '../stim/no_rate/tosho_113.mp3', dur: 113, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_130.mp3', dur: 130, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_147.mp3', dur: 147, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_163.mp3', dur: 163, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_180.mp3', dur: 180, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_197.mp3', dur: 197, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_213.mp3', dur: 213, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_230.mp3', dur: 230, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_80.mp3', dur: 80, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tosho_97.mp3', dur: 97, opt: tosho, orig: 'tosho' },
        { audio: '../stim/no_rate/tousho_113.mp3', dur: 113, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_130.mp3', dur: 130, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_147.mp3', dur: 147, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_163.mp3', dur: 163, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_180.mp3', dur: 180, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_197.mp3', dur: 197, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_213.mp3', dur: 213, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_230.mp3', dur: 230, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_80.mp3', dur: 80, opt: tosho, orig: 'tousho' },
        { audio: '../stim/no_rate/tousho_97.mp3', dur: 97, opt: tosho, orig: 'tousho' }
    ],
    randomize_order: true,
    data: {
        file: jsPsych.timelineVariable('audio'),
        duration: jsPsych.timelineVariable('dur'),
        original_word: jsPsych.timelineVariable('orig')
    }
};

// ending page

var ending = {
    type: 'html-keyboard-response',
    stimulus: "<p>実験に参加して下さってありがとうございます。</p>" +
    "<p>データを保存するにはスペースバーを押して下さい。</p>" +
    "<p>データが保存されたら、認証コードが表示されます。",
    response_ends_trial: true,
    choices: [' '],
    on_finish: function(){
        jsPsych.endExperiment(
            "<p>データが保存されました。</p>" +
            "<p> 認証コードは「" +
            my_id +
            "」です。そのコードをクラウドワークスに入力してください。</p>" +
            "<p>コードを忘れないように書き留めてください。</p>" +
            "<p>コードを書き留めたら、このウィンドーを閉じることができます。"
        );
    }
};

// timeline

var timeline = [];
timeline.push(welcome);
timeline.push(consent);
timeline.push(check_consent);
timeline.push(demographic_questions);
timeline.push(instructions);
timeline.push(test_procedure);
timeline.push(ending);

// preload audio

var audiopaths = [
    '../stim/no_rate/biiru_100.mp3'  , '../stim/no_rate/biiru_114.mp3'  ,
    '../stim/no_rate/biiru_129.mp3'  , '../stim/no_rate/biiru_143.mp3'  ,
    '../stim/no_rate/biiru_158.mp3'  , '../stim/no_rate/biiru_172.mp3'  ,
    '../stim/no_rate/biiru_187.mp3'  , '../stim/no_rate/biiru_201.mp3'  ,
    '../stim/no_rate/biiru_216.mp3'  , '../stim/no_rate/biiru_230.mp3'  ,
    '../stim/no_rate/biru_100.mp3'   , '../stim/no_rate/biru_114.mp3'   ,
    '../stim/no_rate/biru_129.mp3'   , '../stim/no_rate/biru_143.mp3'   ,
    '../stim/no_rate/biru_158.mp3'   , '../stim/no_rate/biru_172.mp3'   ,
    '../stim/no_rate/biru_187.mp3'   , '../stim/no_rate/biru_201.mp3'   ,
    '../stim/no_rate/biru_216.mp3'   , '../stim/no_rate/biru_230.mp3'   ,
    '../stim/no_rate/ika_113.mp3'    , '../stim/no_rate/ika_130.mp3'    ,
    '../stim/no_rate/ika_147.mp3'    , '../stim/no_rate/ika_163.mp3'    ,
    '../stim/no_rate/ika_180.mp3'    , '../stim/no_rate/ika_197.mp3'    ,
    '../stim/no_rate/ika_213.mp3'    , '../stim/no_rate/ika_230.mp3'    ,
    '../stim/no_rate/ika_80.mp3'     , '../stim/no_rate/ika_97.mp3'     ,
    '../stim/no_rate/ikka_113.mp3'   , '../stim/no_rate/ikka_130.mp3'   ,
    '../stim/no_rate/ikka_147.mp3'   , '../stim/no_rate/ikka_163.mp3'   ,
    '../stim/no_rate/ikka_180.mp3'   , '../stim/no_rate/ikka_197.mp3'   ,
    '../stim/no_rate/ikka_213.mp3'   , '../stim/no_rate/ikka_230.mp3'   ,
    '../stim/no_rate/ikka_80.mp3'    , '../stim/no_rate/ikka_97.mp3'    ,
    '../stim/no_rate/juri_104.mp3'   , '../stim/no_rate/juri_117.mp3'   ,
    '../stim/no_rate/juri_129.mp3'   , '../stim/no_rate/juri_141.mp3'   ,
    '../stim/no_rate/juri_153.mp3'   , '../stim/no_rate/juri_166.mp3'   ,
    '../stim/no_rate/juri_178.mp3'   , '../stim/no_rate/juri_190.mp3'   ,
    '../stim/no_rate/juri_80.mp3'    , '../stim/no_rate/juri_92.mp3'    ,
    '../stim/no_rate/juuri_104.mp3'  , '../stim/no_rate/juuri_117.mp3'  ,
    '../stim/no_rate/juuri_129.mp3'  , '../stim/no_rate/juuri_141.mp3'  ,
    '../stim/no_rate/juuri_153.mp3'  , '../stim/no_rate/juuri_166.mp3'  ,
    '../stim/no_rate/juuri_178.mp3'  , '../stim/no_rate/juuri_190.mp3'  ,
    '../stim/no_rate/juuri_80.mp3'   , '../stim/no_rate/juuri_92.mp3'   ,
    '../stim/no_rate/kaado_106.mp3'  , '../stim/no_rate/kaado_121.mp3'  ,
    '../stim/no_rate/kaado_137.mp3'  , '../stim/no_rate/kaado_152.mp3'  ,
    '../stim/no_rate/kaado_168.mp3'  , '../stim/no_rate/kaado_183.mp3'  ,
    '../stim/no_rate/kaado_199.mp3'  , '../stim/no_rate/kaado_214.mp3'  ,
    '../stim/no_rate/kaado_230.mp3'  , '../stim/no_rate/kaado_90.mp3'   ,
    '../stim/no_rate/kado_106.mp3'   , '../stim/no_rate/kado_121.mp3'   ,
    '../stim/no_rate/kado_137.mp3'   , '../stim/no_rate/kado_152.mp3'   ,
    '../stim/no_rate/kado_168.mp3'   , '../stim/no_rate/kado_183.mp3'   ,
    '../stim/no_rate/kado_199.mp3'   , '../stim/no_rate/kado_214.mp3'   ,
    '../stim/no_rate/kado_230.mp3'   , '../stim/no_rate/kado_90.mp3'    ,
    '../stim/no_rate/kakko_103.mp3'  , '../stim/no_rate/kakko_120.mp3'  ,
    '../stim/no_rate/kakko_137.mp3'  , '../stim/no_rate/kakko_153.mp3'  ,
    '../stim/no_rate/kakko_170.mp3'  , '../stim/no_rate/kakko_187.mp3'  ,
    '../stim/no_rate/kakko_203.mp3'  , '../stim/no_rate/kakko_220.mp3'  ,
    '../stim/no_rate/kakko_70.mp3'   , '../stim/no_rate/kakko_87.mp3'   ,
    '../stim/no_rate/kako_103.mp3'   , '../stim/no_rate/kako_120.mp3'   ,
    '../stim/no_rate/kako_137.mp3'   , '../stim/no_rate/kako_153.mp3'   ,
    '../stim/no_rate/kako_170.mp3'   , '../stim/no_rate/kako_187.mp3'   ,
    '../stim/no_rate/kako_203.mp3'   , '../stim/no_rate/kako_220.mp3'   ,
    '../stim/no_rate/kako_70.mp3'    , '../stim/no_rate/kako_87.mp3'    ,
    '../stim/no_rate/medo_110.mp3'   , '../stim/no_rate/medo_123.mp3'   ,
    '../stim/no_rate/medo_137.mp3'   , '../stim/no_rate/medo_150.mp3'   ,
    '../stim/no_rate/medo_163.mp3'   , '../stim/no_rate/medo_177.mp3'   ,
    '../stim/no_rate/medo_190.mp3'   , '../stim/no_rate/medo_203.mp3'   ,
    '../stim/no_rate/medo_217.mp3'   , '../stim/no_rate/medo_230.mp3'   ,
    '../stim/no_rate/meido_110.mp3'  , '../stim/no_rate/meido_123.mp3'  ,
    '../stim/no_rate/meido_137.mp3'  , '../stim/no_rate/meido_150.mp3'  ,
    '../stim/no_rate/meido_163.mp3'  , '../stim/no_rate/meido_177.mp3'  ,
    '../stim/no_rate/meido_190.mp3'  , '../stim/no_rate/meido_203.mp3'  ,
    '../stim/no_rate/meido_217.mp3'  , '../stim/no_rate/meido_230.mp3'  ,
    '../stim/no_rate/meta_113.mp3'   , '../stim/no_rate/meta_130.mp3'   ,
    '../stim/no_rate/meta_147.mp3'   , '../stim/no_rate/meta_163.mp3'   ,
    '../stim/no_rate/meta_180.mp3'   , '../stim/no_rate/meta_197.mp3'   ,
    '../stim/no_rate/meta_213.mp3'   , '../stim/no_rate/meta_230.mp3'   ,
    '../stim/no_rate/meta_80.mp3'    , '../stim/no_rate/meta_97.mp3'    ,
    '../stim/no_rate/metta_113.mp3'  , '../stim/no_rate/metta_130.mp3'  ,
    '../stim/no_rate/metta_147.mp3'  , '../stim/no_rate/metta_163.mp3'  ,
    '../stim/no_rate/metta_180.mp3'  , '../stim/no_rate/metta_197.mp3'  ,
    '../stim/no_rate/metta_213.mp3'  , '../stim/no_rate/metta_230.mp3'  ,
    '../stim/no_rate/metta_80.mp3'   , '../stim/no_rate/metta_97.mp3'   ,
    '../stim/no_rate/tate_110.mp3'   , '../stim/no_rate/tate_133.mp3'   ,
    '../stim/no_rate/tate_157.mp3'   , '../stim/no_rate/tate_180.mp3'   ,
    '../stim/no_rate/tate_203.mp3'   , '../stim/no_rate/tate_227.mp3'   ,
    '../stim/no_rate/tate_250.mp3'   , '../stim/no_rate/tate_40.mp3'    ,
    '../stim/no_rate/tate_63.mp3'    , '../stim/no_rate/tate_87.mp3'    ,
    '../stim/no_rate/tatte_110.mp3'  , '../stim/no_rate/tatte_133.mp3'  ,
    '../stim/no_rate/tatte_157.mp3'  , '../stim/no_rate/tatte_180.mp3'  ,
    '../stim/no_rate/tatte_203.mp3'  , '../stim/no_rate/tatte_227.mp3'  ,
    '../stim/no_rate/tatte_250.mp3'  , '../stim/no_rate/tatte_40.mp3'   ,
    '../stim/no_rate/tatte_63.mp3'   , '../stim/no_rate/tatte_87.mp3'   ,
    '../stim/no_rate/tosho_113.mp3'  , '../stim/no_rate/tosho_130.mp3'  ,
    '../stim/no_rate/tosho_147.mp3'  , '../stim/no_rate/tosho_163.mp3'  ,
    '../stim/no_rate/tosho_180.mp3'  , '../stim/no_rate/tosho_197.mp3'  ,
    '../stim/no_rate/tosho_213.mp3'  , '../stim/no_rate/tosho_230.mp3'  ,
    '../stim/no_rate/tosho_80.mp3'   , '../stim/no_rate/tosho_97.mp3'   ,
    '../stim/no_rate/tousho_113.mp3' , '../stim/no_rate/tousho_130.mp3' ,
    '../stim/no_rate/tousho_147.mp3' , '../stim/no_rate/tousho_163.mp3' ,
    '../stim/no_rate/tousho_180.mp3' , '../stim/no_rate/tousho_197.mp3' ,
    '../stim/no_rate/tousho_213.mp3' , '../stim/no_rate/tousho_230.mp3' ,
    '../stim/no_rate/tousho_80.mp3'  , '../stim/no_rate/tousho_97.mp3'
];

// save data function

function saveData(name, data){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data.php');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({filename: name, filedata: data}));
}

// run jsPsych

jsPsych.init({
    timeline: timeline,
    default_iti: 50,
    preload_audio: audiopaths,
    on_finish: function() {
        if (should_i_save === 1){
            var my_data = jsPsych.data.get().csv();
            var my_data_name = my_id + "_results";
            saveData(my_data_name, my_data);
        }
    }
});
