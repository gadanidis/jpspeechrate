/* globals jsPsych */

var timeline = [];
var my_id = jsPsych.randomization.randomID(8);
var should_i_save = 1;
var completed_trials = 0;

// welcome page
var welcome = {
    type: 'html-keyboard-response',
    stimulus: `実験にようこそ。準備ができたら何でもキーを押してください。`
};

// Consent process

var agree = "私はこの承諾書の内容を理解し、" +
            "上記の実験に参加することを決断しました。";

var disagree = "私はこの承諾書の内容を理解し、" +
               "上記の実験に参加することを決断しませんでした。";


var consent_options = [agree, disagree];

var consent = {
    type: 'survey-multi-choice',
    preamble: `
<div style='text-align: left;'>
<h1>言語学実験</h1>
<h2>研究プロジェクト参加承諾</h2>

<h3>参加要請とプロジェクト概要</h3>

<p>
    この承諾書は言語学実験への参加を要請するものです。
    ご希望であれば、参加を断ることも可能です。
    あなたの言語知識が研究員にとって科学的関心に値すると判断されました。
    この研究調査に参加するか否かを判断していただく上で、調査の目的、リスク、
    そして利益を知っていただく必要があります。
    この承諾書は以下に、実験の詳細な進め方についての情報を記しています。
    この研究の代表者はトロント大学のカン・ユンジュン（Yoonjung Kang）教授です。
    この研究は、様々な言語環境に応じて、
    日本語話者による音声の知覚の仕方がどのように変化するかを
    調査することを目的としています。
    実験の進め方を理解していただいた段階で、参加するか否かを決定してください。
</p>

<h3> 実験内容 </h3>
<p>
    日本語母語話者によって発話された一連の文章を聴いていただき、
    聴き取った音声に最適である対象の単語を選択していただきます。
</p>

<h3>リスクや不都合</h3>
<p>
    この実験には、予想されうるリスクや不便はありません。
</p>

<h3>利益</h3> 
<p>
    この実験の調査内容から参加者に個人的な利益は発生しませんが、
    その他の人々にとって将来利益となりうる情報が研究員に託されます。
</p>

<h3>報酬</h3> 
<p>
    参加の報酬として、３３０円をお支払いします。
    実験に要する時間は１５〜２５分です。
</p>

<h3>個人情報の保護</h3> 
<p>
    参加者の名前は記録されることはありません。
    よって、後の学術的な発表や出版物に参加者の名前が記載されることはありません。
    この調査において集められたその他の個人情報はこの実験の研究員らや
    その共同研究者以外の誰にも開示されることはありません。 
    さらに、これらの情報は可能な限り安全な方法で管理され、
    調査結果が発表された後に削除する配慮がなされる見込みです。
    しかし、言語学研究の標準的な慣例に沿って、（参加者からの特別な依頼がないかぎり）
    収録された言語データは処分されません。
    なぜなら、より広範囲の言語話者のサンプルを含む更なる調査や、
    世代間における言語的変遷を調べる調査に、
    その保存されたデータが必要になる可能性があるためです。
    あなたの参加するこの研究調査は、
    関係する法律や規定が順守されているかを確認する優良性保証のための監査対象となります。
    もし選ばれた場合、Human Research Ethics Program （HREP）もしくは、
    人間を対象とする研究倫理プログラム の代表者に、
    監査の一環として、この調査に関係するデータや承諾書の内容を開示することがあります。
    HREPに開示されるすべての情報は、
    研究チームが上記すると同様の個人情報保護の配慮がなされます。
</p>

<h3>任意参加</h3> 
<p>
    あなたには、参加しないことを選択する権利があります。
    もし参加することを選択しても、どの段階においても、
    途中で実験を辞退することが可能です。
    参加を辞退した場合でも、将来実験に参加する権利を脅かすことはありません。
</p>

<h3>質問</h3> 
<p>
    この承諾書に署名する前に、参加するか否かを決断するのに必要な質問があれば、
    研究員にしてください。
    しかし、実験結果を左右しうる質問の答えは保留させていただきます。
    この実験に参加するか否かの判断に必要な時間は幾分にとっていただいて構いません。
</p>

<h3>問い合わせ先</h3>
<p>
    このプロジェクトに関してさらに質問がある場合は、
    研究代表カン・ユンジュン教授 （416-287-7172もしくはkang@utsc.utoronto.ca）
    に連絡してください。
    研究参加者としてのご自身の権利に関して質問がある場合は、
    the Office of Research Ethics（研究倫理オフィス、
    416-946-3273または ethics.review@utoronto.ca）に連絡してください。
</p>
      `,
    questions: [{
        prompt: "<h3>承諾表明</h3>",
        options: consent_options,
        required: true
    }],
    error_message: "この項目は必須です。",
    button_label: "次へ",
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        jsPsych.data.addProperties({ consent: responses.Q0 });
    }
};

var no_consent = {
    type: 'html-keyboard-response',
    stimulus: "お時間をいただきありがとうございました。",
    on_finish: function(){
        if (true){
            should_i_save = 0;
            jsPsych.endExperiment("お時間をいただきありがとうございます。");
        }
    }
};

var check_consent = {
    timeline: [no_consent],
    conditional_function: function(){
        var data = jsPsych.data.getLastTrialData().values()[0];
        var responses = JSON.parse(data.responses);
        if (responses.Q0 === disagree){
            return true;
        } else {
            return false;
        }
    }
};

// Ask the participant for demo info and save it to data
var demographic_questions = {
    type: 'survey-text',
    button_label: "次へ",
    error_message: "この項目は必須です。",
    questions: [
        {prompt: "生年", required: true},
        {prompt: "性別", required: true},
        {prompt: "出身の都道府県", required: true},
        {prompt: "話せる言語（日本語以外）", required: true}
    ],
    on_finish: function(data){
        var responses = JSON.parse(data.responses);
        var age = responses.Q0;
        var my_age = age;
        var gender = responses.Q1;
        var my_gender = gender;
        var origin = responses.Q2;
        var my_origin = origin;
        var languages = responses.Q3;
        var my_languages = languages;
        jsPsych.data.addProperties({
            subject_id: my_id,
            gender: my_gender,
            age: my_age,
            origin: my_origin,
            languages: my_languages}
        );
    }
};

// Instructions

var instructions = {
    type: 'html-keyboard-response',
    stimulus: "<p> この実験では日本語の話者が「竹内さんはとても穏やかに◯◯を発音した」" +
        "と言うのが聞こえます。 </p>" +
        "<p> ０か１のキーを押すと、竹内さんが発音した単語を選んで下さい。 </p>" +
        "<p> 準備ができたらスペースバーを押してください。 </p>",
    choices: [' ']
};


// Scales for test
var nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

var meta  = "１：メタ"   + nbsp + "０：滅多";
var tate  = "１：縦"     + nbsp + "０：立って";
var ika   = "１：以下"   + nbsp + "０：一家";
var kako  = "１：過去"   + nbsp + "０：括弧";

var kado  = "１：角"     + nbsp + "０：カード";
var biru  = "１：ビル"   + nbsp + "０：ビール";
var medo  = "１：目処"   + nbsp + "０：明度";
var tosho = "１：図書"   + nbsp + "０：当初";

// Test procedure
var trial = {
    type: 'audio-keyboard-response',
    choices: ['0', '1'],
    prompt: jsPsych.timelineVariable('opt'),
    stimulus: jsPsych.timelineVariable('audio'),
    on_finish: function(data){
        if (data.key_press === 48) {
            data.response = "long";
        } else {
            data.response = "short";
        }
        completed_trials += 1;
    }
};

var take_a_break = {
    type: 'html-keyboard-response',
    stimulus: `ちょっと休憩できます。
               続行する準備ができたら、
               スペースバーを押してください。`,
    choices: (' ')
};

var maybe_break = {
    timeline: [take_a_break],
    conditional_function: function(){
        if (completed_trials % 32 === 0) {
            return true;
        } else {
            return false;
        }
    }
};

var test_procedure = {
    timeline: [trial, maybe_break],
    timeline_variables: [
        { audio: '../stim/samediff/biiru_fast_diff_20_c0_v36_142.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'diff' , dur: 142 },
        { audio: '../stim/samediff/biiru_fast_diff_20_c0_v36_170.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'diff' , dur: 170 },
        { audio: '../stim/samediff/biiru_fast_diff_20_c0_v36_197.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'diff' , dur: 197 },
        { audio: '../stim/samediff/biiru_fast_diff_20_c0_v36_260.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'diff' , dur: 260 },
        { audio: '../stim/samediff/biiru_fast_diff_20_c0_v36_80.mp3'   , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'diff' , dur: 80  },
        { audio: '../stim/samediff/biiru_fast_same_20_142.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'same' , dur: 142 },
        { audio: '../stim/samediff/biiru_fast_same_20_170.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'same' , dur: 170 },
        { audio: '../stim/samediff/biiru_fast_same_20_197.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'same' , dur: 197 },
        { audio: '../stim/samediff/biiru_fast_same_20_260.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'same' , dur: 260 },
        { audio: '../stim/samediff/biiru_fast_same_20_80.mp3'          , opt: biru  , orig: 'biiru'  , rate: 'fast' , bal: 'same' , dur: 80  },
        { audio: '../stim/samediff/biiru_slow_diff_20_c0_v36_142.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'diff' , dur: 142 },
        { audio: '../stim/samediff/biiru_slow_diff_20_c0_v36_170.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'diff' , dur: 170 },
        { audio: '../stim/samediff/biiru_slow_diff_20_c0_v36_197.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'diff' , dur: 197 },
        { audio: '../stim/samediff/biiru_slow_diff_20_c0_v36_260.mp3'  , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'diff' , dur: 260 },
        { audio: '../stim/samediff/biiru_slow_diff_20_c0_v36_80.mp3'   , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'diff' , dur: 80  },
        { audio: '../stim/samediff/biiru_slow_same_20_142.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'same' , dur: 142 },
        { audio: '../stim/samediff/biiru_slow_same_20_170.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'same' , dur: 170 },
        { audio: '../stim/samediff/biiru_slow_same_20_197.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'same' , dur: 197 },
        { audio: '../stim/samediff/biiru_slow_same_20_260.mp3'         , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'same' , dur: 260 },
        { audio: '../stim/samediff/biiru_slow_same_20_80.mp3'          , opt: biru  , orig: 'biiru'  , rate: 'slow' , bal: 'same' , dur: 80  },
        { audio: '../stim/samediff/biru_fast_diff_20_c0_v36_142.mp3'   , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'diff' , dur: 142 },
        { audio: '../stim/samediff/biru_fast_diff_20_c0_v36_170.mp3'   , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'diff' , dur: 170 },
        { audio: '../stim/samediff/biru_fast_diff_20_c0_v36_197.mp3'   , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'diff' , dur: 197 },
        { audio: '../stim/samediff/biru_fast_diff_20_c0_v36_260.mp3'   , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'diff' , dur: 260 },
        { audio: '../stim/samediff/biru_fast_diff_20_c0_v36_80.mp3'    , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'diff' , dur: 80  },
        { audio: '../stim/samediff/biru_fast_same_20_142.mp3'          , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'same' , dur: 142 },
        { audio: '../stim/samediff/biru_fast_same_20_170.mp3'          , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'same' , dur: 170 },
        { audio: '../stim/samediff/biru_fast_same_20_197.mp3'          , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'same' , dur: 197 },
        { audio: '../stim/samediff/biru_fast_same_20_260.mp3'          , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'same' , dur: 260 },
        { audio: '../stim/samediff/biru_fast_same_20_80.mp3'           , opt: biru  , orig: 'biru'   , rate: 'fast' , bal: 'same' , dur: 80  },
        { audio: '../stim/samediff/biru_slow_diff_20_c0_v36_142.mp3'   , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'diff' , dur: 142 },
        { audio: '../stim/samediff/biru_slow_diff_20_c0_v36_170.mp3'   , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'diff' , dur: 170 },
        { audio: '../stim/samediff/biru_slow_diff_20_c0_v36_197.mp3'   , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'diff' , dur: 197 },
        { audio: '../stim/samediff/biru_slow_diff_20_c0_v36_260.mp3'   , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'diff' , dur: 260 },
        { audio: '../stim/samediff/biru_slow_diff_20_c0_v36_80.mp3'    , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'diff' , dur: 80  },
        { audio: '../stim/samediff/biru_slow_same_20_142.mp3'          , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'same' , dur: 142 },
        { audio: '../stim/samediff/biru_slow_same_20_170.mp3'          , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'same' , dur: 170 },
        { audio: '../stim/samediff/biru_slow_same_20_197.mp3'          , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'same' , dur: 197 },
        { audio: '../stim/samediff/biru_slow_same_20_260.mp3'          , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'same' , dur: 260 },
        { audio: '../stim/samediff/biru_slow_same_20_80.mp3'           , opt: biru  , orig: 'biru'   , rate: 'slow' , bal: 'same' , dur: 80  },
        { audio: '../stim/samediff/ika_fast_diff_20_c0_v36_110.mp3'    , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/ika_fast_diff_20_c0_v36_127.mp3'    , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/ika_fast_diff_20_c0_v36_145.mp3'    , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'diff' , dur: 145 },
        { audio: '../stim/samediff/ika_fast_diff_20_c0_v36_184.mp3'    , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'diff' , dur: 184 },
        { audio: '../stim/samediff/ika_fast_diff_20_c0_v36_70.mp3'     , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'diff' , dur: 70  },
        { audio: '../stim/samediff/ika_fast_same_20_110.mp3'           , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/ika_fast_same_20_127.mp3'           , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/ika_fast_same_20_145.mp3'           , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'same' , dur: 145 },
        { audio: '../stim/samediff/ika_fast_same_20_184.mp3'           , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'same' , dur: 184 },
        { audio: '../stim/samediff/ika_fast_same_20_70.mp3'            , opt: ika   , orig: 'ika'    , rate: 'fast' , bal: 'same' , dur: 70  },
        { audio: '../stim/samediff/ika_slow_diff_20_c0_v36_110.mp3'    , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/ika_slow_diff_20_c0_v36_127.mp3'    , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/ika_slow_diff_20_c0_v36_145.mp3'    , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'diff' , dur: 145 },
        { audio: '../stim/samediff/ika_slow_diff_20_c0_v36_184.mp3'    , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'diff' , dur: 184 },
        { audio: '../stim/samediff/ika_slow_diff_20_c0_v36_70.mp3'     , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'diff' , dur: 70  },
        { audio: '../stim/samediff/ika_slow_same_20_110.mp3'           , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/ika_slow_same_20_127.mp3'           , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/ika_slow_same_20_145.mp3'           , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'same' , dur: 145 },
        { audio: '../stim/samediff/ika_slow_same_20_184.mp3'           , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'same' , dur: 184 },
        { audio: '../stim/samediff/ika_slow_same_20_70.mp3'            , opt: ika   , orig: 'ika'    , rate: 'slow' , bal: 'same' , dur: 70  },
        { audio: '../stim/samediff/ikka_fast_diff_20_c0_v36_110.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/ikka_fast_diff_20_c0_v36_127.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/ikka_fast_diff_20_c0_v36_145.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'diff' , dur: 145 },
        { audio: '../stim/samediff/ikka_fast_diff_20_c0_v36_184.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'diff' , dur: 184 },
        { audio: '../stim/samediff/ikka_fast_diff_20_c0_v36_70.mp3'    , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'diff' , dur: 70  },
        { audio: '../stim/samediff/ikka_fast_same_20_110.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/ikka_fast_same_20_127.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/ikka_fast_same_20_145.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'same' , dur: 145 },
        { audio: '../stim/samediff/ikka_fast_same_20_184.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'same' , dur: 184 },
        { audio: '../stim/samediff/ikka_fast_same_20_70.mp3'           , opt: ika   , orig: 'ikka'   , rate: 'fast' , bal: 'same' , dur: 70  },
        { audio: '../stim/samediff/ikka_slow_diff_20_c0_v36_110.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/ikka_slow_diff_20_c0_v36_127.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/ikka_slow_diff_20_c0_v36_145.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'diff' , dur: 145 },
        { audio: '../stim/samediff/ikka_slow_diff_20_c0_v36_184.mp3'   , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'diff' , dur: 184 },
        { audio: '../stim/samediff/ikka_slow_diff_20_c0_v36_70.mp3'    , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'diff' , dur: 70  },
        { audio: '../stim/samediff/ikka_slow_same_20_110.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/ikka_slow_same_20_127.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/ikka_slow_same_20_145.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'same' , dur: 145 },
        { audio: '../stim/samediff/ikka_slow_same_20_184.mp3'          , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'same' , dur: 184 },
        { audio: '../stim/samediff/ikka_slow_same_20_70.mp3'           , opt: ika   , orig: 'ikka'   , rate: 'slow' , bal: 'same' , dur: 70  },
        { audio: '../stim/samediff/kaado_fast_diff_20_c0_v36_132.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'diff' , dur: 132 },
        { audio: '../stim/samediff/kaado_fast_diff_20_c0_v36_151.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'diff' , dur: 151 },
        { audio: '../stim/samediff/kaado_fast_diff_20_c0_v36_169.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'diff' , dur: 169 },
        { audio: '../stim/samediff/kaado_fast_diff_20_c0_v36_212.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'diff' , dur: 212 },
        { audio: '../stim/samediff/kaado_fast_diff_20_c0_v36_89.mp3'   , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/kaado_fast_same_20_132.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'same' , dur: 132 },
        { audio: '../stim/samediff/kaado_fast_same_20_151.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'same' , dur: 151 },
        { audio: '../stim/samediff/kaado_fast_same_20_169.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'same' , dur: 169 },
        { audio: '../stim/samediff/kaado_fast_same_20_212.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'same' , dur: 212 },
        { audio: '../stim/samediff/kaado_fast_same_20_89.mp3'          , opt: kado  , orig: 'kaado'  , rate: 'fast' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/kaado_slow_diff_20_c0_v36_132.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'diff' , dur: 132 },
        { audio: '../stim/samediff/kaado_slow_diff_20_c0_v36_151.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'diff' , dur: 151 },
        { audio: '../stim/samediff/kaado_slow_diff_20_c0_v36_169.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'diff' , dur: 169 },
        { audio: '../stim/samediff/kaado_slow_diff_20_c0_v36_212.mp3'  , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'diff' , dur: 212 },
        { audio: '../stim/samediff/kaado_slow_diff_20_c0_v36_89.mp3'   , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/kaado_slow_same_20_132.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'same' , dur: 132 },
        { audio: '../stim/samediff/kaado_slow_same_20_151.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'same' , dur: 151 },
        { audio: '../stim/samediff/kaado_slow_same_20_169.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'same' , dur: 169 },
        { audio: '../stim/samediff/kaado_slow_same_20_212.mp3'         , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'same' , dur: 212 },
        { audio: '../stim/samediff/kaado_slow_same_20_89.mp3'          , opt: kado  , orig: 'kaado'  , rate: 'slow' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/kado_fast_diff_20_c0_v36_132.mp3'   , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'diff' , dur: 132 },
        { audio: '../stim/samediff/kado_fast_diff_20_c0_v36_151.mp3'   , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'diff' , dur: 151 },
        { audio: '../stim/samediff/kado_fast_diff_20_c0_v36_169.mp3'   , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'diff' , dur: 169 },
        { audio: '../stim/samediff/kado_fast_diff_20_c0_v36_212.mp3'   , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'diff' , dur: 212 },
        { audio: '../stim/samediff/kado_fast_diff_20_c0_v36_89.mp3'    , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/kado_fast_same_20_132.mp3'          , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'same' , dur: 132 },
        { audio: '../stim/samediff/kado_fast_same_20_151.mp3'          , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'same' , dur: 151 },
        { audio: '../stim/samediff/kado_fast_same_20_169.mp3'          , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'same' , dur: 169 },
        { audio: '../stim/samediff/kado_fast_same_20_212.mp3'          , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'same' , dur: 212 },
        { audio: '../stim/samediff/kado_fast_same_20_89.mp3'           , opt: kado  , orig: 'kado'   , rate: 'fast' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/kado_slow_diff_20_c0_v36_132.mp3'   , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'diff' , dur: 132 },
        { audio: '../stim/samediff/kado_slow_diff_20_c0_v36_151.mp3'   , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'diff' , dur: 151 },
        { audio: '../stim/samediff/kado_slow_diff_20_c0_v36_169.mp3'   , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'diff' , dur: 169 },
        { audio: '../stim/samediff/kado_slow_diff_20_c0_v36_212.mp3'   , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'diff' , dur: 212 },
        { audio: '../stim/samediff/kado_slow_diff_20_c0_v36_89.mp3'    , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/kado_slow_same_20_132.mp3'          , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'same' , dur: 132 },
        { audio: '../stim/samediff/kado_slow_same_20_151.mp3'          , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'same' , dur: 151 },
        { audio: '../stim/samediff/kado_slow_same_20_169.mp3'          , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'same' , dur: 169 },
        { audio: '../stim/samediff/kado_slow_same_20_212.mp3'          , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'same' , dur: 212 },
        { audio: '../stim/samediff/kado_slow_same_20_89.mp3'           , opt: kado  , orig: 'kado'   , rate: 'slow' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/kakko_fast_diff_20_c0_v36_109.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'diff' , dur: 109 },
        { audio: '../stim/samediff/kakko_fast_diff_20_c0_v36_124.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'diff' , dur: 124 },
        { audio: '../stim/samediff/kakko_fast_diff_20_c0_v36_140.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'diff' , dur: 140 },
        { audio: '../stim/samediff/kakko_fast_diff_20_c0_v36_174.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'diff' , dur: 174 },
        { audio: '../stim/samediff/kakko_fast_diff_20_c0_v36_74.mp3'   , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'diff' , dur: 74  },
        { audio: '../stim/samediff/kakko_fast_same_20_109.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'same' , dur: 109 },
        { audio: '../stim/samediff/kakko_fast_same_20_124.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'same' , dur: 124 },
        { audio: '../stim/samediff/kakko_fast_same_20_140.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'same' , dur: 140 },
        { audio: '../stim/samediff/kakko_fast_same_20_174.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'same' , dur: 174 },
        { audio: '../stim/samediff/kakko_fast_same_20_74.mp3'          , opt: kako  , orig: 'kakko'  , rate: 'fast' , bal: 'same' , dur: 74  },
        { audio: '../stim/samediff/kakko_slow_diff_20_c0_v36_109.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'diff' , dur: 109 },
        { audio: '../stim/samediff/kakko_slow_diff_20_c0_v36_124.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'diff' , dur: 124 },
        { audio: '../stim/samediff/kakko_slow_diff_20_c0_v36_140.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'diff' , dur: 140 },
        { audio: '../stim/samediff/kakko_slow_diff_20_c0_v36_174.mp3'  , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'diff' , dur: 174 },
        { audio: '../stim/samediff/kakko_slow_diff_20_c0_v36_74.mp3'   , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'diff' , dur: 74  },
        { audio: '../stim/samediff/kakko_slow_same_20_109.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'same' , dur: 109 },
        { audio: '../stim/samediff/kakko_slow_same_20_124.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'same' , dur: 124 },
        { audio: '../stim/samediff/kakko_slow_same_20_140.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'same' , dur: 140 },
        { audio: '../stim/samediff/kakko_slow_same_20_174.mp3'         , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'same' , dur: 174 },
        { audio: '../stim/samediff/kakko_slow_same_20_74.mp3'          , opt: kako  , orig: 'kakko'  , rate: 'slow' , bal: 'same' , dur: 74  },
        { audio: '../stim/samediff/kako_fast_diff_20_c0_v36_109.mp3'   , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'diff' , dur: 109 },
        { audio: '../stim/samediff/kako_fast_diff_20_c0_v36_124.mp3'   , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'diff' , dur: 124 },
        { audio: '../stim/samediff/kako_fast_diff_20_c0_v36_140.mp3'   , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'diff' , dur: 140 },
        { audio: '../stim/samediff/kako_fast_diff_20_c0_v36_174.mp3'   , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'diff' , dur: 174 },
        { audio: '../stim/samediff/kako_fast_diff_20_c0_v36_74.mp3'    , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'diff' , dur: 74  },
        { audio: '../stim/samediff/kako_fast_same_20_109.mp3'          , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'same' , dur: 109 },
        { audio: '../stim/samediff/kako_fast_same_20_124.mp3'          , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'same' , dur: 124 },
        { audio: '../stim/samediff/kako_fast_same_20_140.mp3'          , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'same' , dur: 140 },
        { audio: '../stim/samediff/kako_fast_same_20_174.mp3'          , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'same' , dur: 174 },
        { audio: '../stim/samediff/kako_fast_same_20_74.mp3'           , opt: kako  , orig: 'kako'   , rate: 'fast' , bal: 'same' , dur: 74  },
        { audio: '../stim/samediff/kako_slow_diff_20_c0_v36_109.mp3'   , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'diff' , dur: 109 },
        { audio: '../stim/samediff/kako_slow_diff_20_c0_v36_124.mp3'   , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'diff' , dur: 124 },
        { audio: '../stim/samediff/kako_slow_diff_20_c0_v36_140.mp3'   , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'diff' , dur: 140 },
        { audio: '../stim/samediff/kako_slow_diff_20_c0_v36_174.mp3'   , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'diff' , dur: 174 },
        { audio: '../stim/samediff/kako_slow_diff_20_c0_v36_74.mp3'    , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'diff' , dur: 74  },
        { audio: '../stim/samediff/kako_slow_same_20_109.mp3'          , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'same' , dur: 109 },
        { audio: '../stim/samediff/kako_slow_same_20_124.mp3'          , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'same' , dur: 124 },
        { audio: '../stim/samediff/kako_slow_same_20_140.mp3'          , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'same' , dur: 140 },
        { audio: '../stim/samediff/kako_slow_same_20_174.mp3'          , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'same' , dur: 174 },
        { audio: '../stim/samediff/kako_slow_same_20_74.mp3'           , opt: kako  , orig: 'kako'   , rate: 'slow' , bal: 'same' , dur: 74  },
        { audio: '../stim/samediff/medo_fast_diff_20_c0_v36_148.mp3'   , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/medo_fast_diff_20_c0_v36_175.mp3'   , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'diff' , dur: 175 },
        { audio: '../stim/samediff/medo_fast_diff_20_c0_v36_201.mp3'   , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'diff' , dur: 201 },
        { audio: '../stim/samediff/medo_fast_diff_20_c0_v36_261.mp3'   , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'diff' , dur: 261 },
        { audio: '../stim/samediff/medo_fast_diff_20_c0_v36_89.mp3'    , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/medo_fast_same_20_148.mp3'          , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/medo_fast_same_20_175.mp3'          , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'same' , dur: 175 },
        { audio: '../stim/samediff/medo_fast_same_20_201.mp3'          , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'same' , dur: 201 },
        { audio: '../stim/samediff/medo_fast_same_20_261.mp3'          , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'same' , dur: 261 },
        { audio: '../stim/samediff/medo_fast_same_20_89.mp3'           , opt: medo  , orig: 'medo'   , rate: 'fast' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/medo_slow_diff_20_c0_v36_148.mp3'   , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/medo_slow_diff_20_c0_v36_175.mp3'   , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'diff' , dur: 175 },
        { audio: '../stim/samediff/medo_slow_diff_20_c0_v36_201.mp3'   , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'diff' , dur: 201 },
        { audio: '../stim/samediff/medo_slow_diff_20_c0_v36_261.mp3'   , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'diff' , dur: 261 },
        { audio: '../stim/samediff/medo_slow_diff_20_c0_v36_89.mp3'    , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/medo_slow_same_20_148.mp3'          , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/medo_slow_same_20_175.mp3'          , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'same' , dur: 175 },
        { audio: '../stim/samediff/medo_slow_same_20_201.mp3'          , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'same' , dur: 201 },
        { audio: '../stim/samediff/medo_slow_same_20_261.mp3'          , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'same' , dur: 261 },
        { audio: '../stim/samediff/medo_slow_same_20_89.mp3'           , opt: medo  , orig: 'medo'   , rate: 'slow' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/meido_fast_diff_20_c0_v36_148.mp3'  , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/meido_fast_diff_20_c0_v36_175.mp3'  , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'diff' , dur: 175 },
        { audio: '../stim/samediff/meido_fast_diff_20_c0_v36_201.mp3'  , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'diff' , dur: 201 },
        { audio: '../stim/samediff/meido_fast_diff_20_c0_v36_261.mp3'  , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'diff' , dur: 261 },
        { audio: '../stim/samediff/meido_fast_diff_20_c0_v36_89.mp3'   , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/meido_fast_same_20_148.mp3'         , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/meido_fast_same_20_175.mp3'         , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'same' , dur: 175 },
        { audio: '../stim/samediff/meido_fast_same_20_201.mp3'         , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'same' , dur: 201 },
        { audio: '../stim/samediff/meido_fast_same_20_261.mp3'         , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'same' , dur: 261 },
        { audio: '../stim/samediff/meido_fast_same_20_89.mp3'          , opt: medo  , orig: 'meido'  , rate: 'fast' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/meido_slow_diff_20_c0_v36_148.mp3'  , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/meido_slow_diff_20_c0_v36_175.mp3'  , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'diff' , dur: 175 },
        { audio: '../stim/samediff/meido_slow_diff_20_c0_v36_201.mp3'  , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'diff' , dur: 201 },
        { audio: '../stim/samediff/meido_slow_diff_20_c0_v36_261.mp3'  , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'diff' , dur: 261 },
        { audio: '../stim/samediff/meido_slow_diff_20_c0_v36_89.mp3'   , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'diff' , dur: 89  },
        { audio: '../stim/samediff/meido_slow_same_20_148.mp3'         , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/meido_slow_same_20_175.mp3'         , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'same' , dur: 175 },
        { audio: '../stim/samediff/meido_slow_same_20_201.mp3'         , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'same' , dur: 201 },
        { audio: '../stim/samediff/meido_slow_same_20_261.mp3'         , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'same' , dur: 261 },
        { audio: '../stim/samediff/meido_slow_same_20_89.mp3'          , opt: medo  , orig: 'meido'  , rate: 'slow' , bal: 'same' , dur: 89  },
        { audio: '../stim/samediff/meta_fast_diff_20_c0_v36_110.mp3'   , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/meta_fast_diff_20_c0_v36_129.mp3'   , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'diff' , dur: 129 },
        { audio: '../stim/samediff/meta_fast_diff_20_c0_v36_148.mp3'   , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/meta_fast_diff_20_c0_v36_191.mp3'   , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'diff' , dur: 191 },
        { audio: '../stim/samediff/meta_fast_diff_20_c0_v36_66.mp3'    , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'diff' , dur: 66  },
        { audio: '../stim/samediff/meta_fast_same_20_110.mp3'          , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/meta_fast_same_20_129.mp3'          , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'same' , dur: 129 },
        { audio: '../stim/samediff/meta_fast_same_20_148.mp3'          , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/meta_fast_same_20_191.mp3'          , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'same' , dur: 191 },
        { audio: '../stim/samediff/meta_fast_same_20_66.mp3'           , opt: meta  , orig: 'meta'   , rate: 'fast' , bal: 'same' , dur: 66  },
        { audio: '../stim/samediff/meta_slow_diff_20_c0_v36_110.mp3'   , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/meta_slow_diff_20_c0_v36_129.mp3'   , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'diff' , dur: 129 },
        { audio: '../stim/samediff/meta_slow_diff_20_c0_v36_148.mp3'   , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/meta_slow_diff_20_c0_v36_191.mp3'   , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'diff' , dur: 191 },
        { audio: '../stim/samediff/meta_slow_diff_20_c0_v36_66.mp3'    , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'diff' , dur: 66  },
        { audio: '../stim/samediff/meta_slow_same_20_110.mp3'          , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/meta_slow_same_20_129.mp3'          , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'same' , dur: 129 },
        { audio: '../stim/samediff/meta_slow_same_20_148.mp3'          , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/meta_slow_same_20_191.mp3'          , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'same' , dur: 191 },
        { audio: '../stim/samediff/meta_slow_same_20_66.mp3'           , opt: meta  , orig: 'meta'   , rate: 'slow' , bal: 'same' , dur: 66  },
        { audio: '../stim/samediff/metta_fast_diff_20_c0_v36_110.mp3'  , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/metta_fast_diff_20_c0_v36_129.mp3'  , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'diff' , dur: 129 },
        { audio: '../stim/samediff/metta_fast_diff_20_c0_v36_148.mp3'  , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/metta_fast_diff_20_c0_v36_191.mp3'  , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'diff' , dur: 191 },
        { audio: '../stim/samediff/metta_fast_diff_20_c0_v36_66.mp3'   , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'diff' , dur: 66  },
        { audio: '../stim/samediff/metta_fast_same_20_110.mp3'         , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/metta_fast_same_20_129.mp3'         , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'same' , dur: 129 },
        { audio: '../stim/samediff/metta_fast_same_20_148.mp3'         , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/metta_fast_same_20_191.mp3'         , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'same' , dur: 191 },
        { audio: '../stim/samediff/metta_fast_same_20_66.mp3'          , opt: meta  , orig: 'metta'  , rate: 'fast' , bal: 'same' , dur: 66  },
        { audio: '../stim/samediff/metta_slow_diff_20_c0_v36_110.mp3'  , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/metta_slow_diff_20_c0_v36_129.mp3'  , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'diff' , dur: 129 },
        { audio: '../stim/samediff/metta_slow_diff_20_c0_v36_148.mp3'  , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'diff' , dur: 148 },
        { audio: '../stim/samediff/metta_slow_diff_20_c0_v36_191.mp3'  , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'diff' , dur: 191 },
        { audio: '../stim/samediff/metta_slow_diff_20_c0_v36_66.mp3'   , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'diff' , dur: 66  },
        { audio: '../stim/samediff/metta_slow_same_20_110.mp3'         , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/metta_slow_same_20_129.mp3'         , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'same' , dur: 129 },
        { audio: '../stim/samediff/metta_slow_same_20_148.mp3'         , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'same' , dur: 148 },
        { audio: '../stim/samediff/metta_slow_same_20_191.mp3'         , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'same' , dur: 191 },
        { audio: '../stim/samediff/metta_slow_same_20_66.mp3'          , opt: meta  , orig: 'metta'  , rate: 'slow' , bal: 'same' , dur: 66  },
        { audio: '../stim/samediff/tate_fast_diff_20_c0_v36_110.mp3'   , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tate_fast_diff_20_c0_v36_127.mp3'   , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tate_fast_diff_20_c0_v36_144.mp3'   , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tate_fast_diff_20_c0_v36_183.mp3'   , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tate_fast_diff_20_c0_v36_72.mp3'    , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tate_fast_same_20_110.mp3'          , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tate_fast_same_20_127.mp3'          , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tate_fast_same_20_144.mp3'          , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tate_fast_same_20_183.mp3'          , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tate_fast_same_20_72.mp3'           , opt: tate  , orig: 'tate'   , rate: 'fast' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tate_slow_diff_20_c0_v36_110.mp3'   , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tate_slow_diff_20_c0_v36_127.mp3'   , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tate_slow_diff_20_c0_v36_144.mp3'   , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tate_slow_diff_20_c0_v36_183.mp3'   , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tate_slow_diff_20_c0_v36_72.mp3'    , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tate_slow_same_20_110.mp3'          , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tate_slow_same_20_127.mp3'          , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tate_slow_same_20_144.mp3'          , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tate_slow_same_20_183.mp3'          , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tate_slow_same_20_72.mp3'           , opt: tate  , orig: 'tate'   , rate: 'slow' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tatte_fast_diff_20_c0_v36_110.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tatte_fast_diff_20_c0_v36_127.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tatte_fast_diff_20_c0_v36_144.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tatte_fast_diff_20_c0_v36_183.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tatte_fast_diff_20_c0_v36_72.mp3'   , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tatte_fast_same_20_110.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tatte_fast_same_20_127.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tatte_fast_same_20_144.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tatte_fast_same_20_183.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tatte_fast_same_20_72.mp3'          , opt: tate  , orig: 'tatte'  , rate: 'fast' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tatte_slow_diff_20_c0_v36_110.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tatte_slow_diff_20_c0_v36_127.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tatte_slow_diff_20_c0_v36_144.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tatte_slow_diff_20_c0_v36_183.mp3'  , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tatte_slow_diff_20_c0_v36_72.mp3'   , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tatte_slow_same_20_110.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tatte_slow_same_20_127.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tatte_slow_same_20_144.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tatte_slow_same_20_183.mp3'         , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tatte_slow_same_20_72.mp3'          , opt: tate  , orig: 'tatte'  , rate: 'slow' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tosho_fast_diff_20_c0_v36_110.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tosho_fast_diff_20_c0_v36_127.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tosho_fast_diff_20_c0_v36_144.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tosho_fast_diff_20_c0_v36_183.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tosho_fast_diff_20_c0_v36_72.mp3'   , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tosho_fast_same_20_110.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tosho_fast_same_20_127.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tosho_fast_same_20_144.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tosho_fast_same_20_183.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tosho_fast_same_20_72.mp3'          , opt: tosho , orig: 'tosho'  , rate: 'fast' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tosho_slow_diff_20_c0_v36_110.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tosho_slow_diff_20_c0_v36_127.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tosho_slow_diff_20_c0_v36_144.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tosho_slow_diff_20_c0_v36_183.mp3'  , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tosho_slow_diff_20_c0_v36_72.mp3'   , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tosho_slow_same_20_110.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tosho_slow_same_20_127.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tosho_slow_same_20_144.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tosho_slow_same_20_183.mp3'         , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tosho_slow_same_20_72.mp3'          , opt: tosho , orig: 'tosho'  , rate: 'slow' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tousho_fast_diff_20_c0_v36_110.mp3' , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tousho_fast_diff_20_c0_v36_127.mp3' , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tousho_fast_diff_20_c0_v36_144.mp3' , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tousho_fast_diff_20_c0_v36_183.mp3' , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tousho_fast_diff_20_c0_v36_72.mp3'  , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tousho_fast_same_20_110.mp3'        , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tousho_fast_same_20_127.mp3'        , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tousho_fast_same_20_144.mp3'        , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tousho_fast_same_20_183.mp3'        , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tousho_fast_same_20_72.mp3'         , opt: tosho , orig: 'tousho' , rate: 'fast' , bal: 'same' , dur: 72  },
        { audio: '../stim/samediff/tousho_slow_diff_20_c0_v36_110.mp3' , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'diff' , dur: 110 },
        { audio: '../stim/samediff/tousho_slow_diff_20_c0_v36_127.mp3' , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'diff' , dur: 127 },
        { audio: '../stim/samediff/tousho_slow_diff_20_c0_v36_144.mp3' , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'diff' , dur: 144 },
        { audio: '../stim/samediff/tousho_slow_diff_20_c0_v36_183.mp3' , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'diff' , dur: 183 },
        { audio: '../stim/samediff/tousho_slow_diff_20_c0_v36_72.mp3'  , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'diff' , dur: 72  },
        { audio: '../stim/samediff/tousho_slow_same_20_110.mp3'        , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'same' , dur: 110 },
        { audio: '../stim/samediff/tousho_slow_same_20_127.mp3'        , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'same' , dur: 127 },
        { audio: '../stim/samediff/tousho_slow_same_20_144.mp3'        , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'same' , dur: 144 },
        { audio: '../stim/samediff/tousho_slow_same_20_183.mp3'        , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'same' , dur: 183 },
        { audio: '../stim/samediff/tousho_slow_same_20_72.mp3'         , opt: tosho , orig: 'tousho' , rate: 'slow' , bal: 'same' , dur: 72  },
    ],
    randomize_order: true,
    data: {
        file: jsPsych.timelineVariable('audio'),
        duration: jsPsych.timelineVariable('dur'),
        original_word: jsPsych.timelineVariable('orig'),
        rate: jsPsych.timelineVariable('rate'),
        bal: jsPsych.timelineVariable('bal')
    }
};

// ending page

var ending = {
    type: 'html-keyboard-response',
    stimulus: "<p>実験に参加して下さってありがとうございます。</p>" +
    "<p>データを保存するにはスペースバーを押して下さい。</p>" +
    "<p>データが保存されたら、認証コードが表示されます。",
    response_ends_trial: true,
    choices: [' '],
    on_finish: function(){
        jsPsych.endExperiment(
            "<p>データが保存されました。</p>" +
            "<p> 認証コードは「" +
            my_id +
            "」です。そのコードをクラウドワークスに入力してください。</p>" +
            "<p>コードを忘れないように書き留めてください。</p>" +
            "<p>コードを書き留めたら、このウィンドーを閉じることができます。"
        );
    }
};

// timeline

var timeline = [];
timeline.push(welcome);
timeline.push(consent);
timeline.push(check_consent);
timeline.push(demographic_questions);
timeline.push(instructions);
timeline.push(test_procedure);
timeline.push(ending);

// preload audio

var audiopaths = [
    '../stim/samediff/biiru_fast_diff_20_c0_v36_142.mp3',
    '../stim/samediff/biiru_fast_diff_20_c0_v36_170.mp3',
    '../stim/samediff/biiru_fast_diff_20_c0_v36_197.mp3',
    '../stim/samediff/biiru_fast_diff_20_c0_v36_260.mp3',
    '../stim/samediff/biiru_fast_diff_20_c0_v36_80.mp3',
    '../stim/samediff/biiru_fast_same_20_142.mp3',
    '../stim/samediff/biiru_fast_same_20_170.mp3',
    '../stim/samediff/biiru_fast_same_20_197.mp3',
    '../stim/samediff/biiru_fast_same_20_260.mp3',
    '../stim/samediff/biiru_fast_same_20_80.mp3',
    '../stim/samediff/biiru_slow_diff_20_c0_v36_142.mp3',
    '../stim/samediff/biiru_slow_diff_20_c0_v36_170.mp3',
    '../stim/samediff/biiru_slow_diff_20_c0_v36_197.mp3',
    '../stim/samediff/biiru_slow_diff_20_c0_v36_260.mp3',
    '../stim/samediff/biiru_slow_diff_20_c0_v36_80.mp3',
    '../stim/samediff/biiru_slow_same_20_142.mp3',
    '../stim/samediff/biiru_slow_same_20_170.mp3',
    '../stim/samediff/biiru_slow_same_20_197.mp3',
    '../stim/samediff/biiru_slow_same_20_260.mp3',
    '../stim/samediff/biiru_slow_same_20_80.mp3',
    '../stim/samediff/biru_fast_diff_20_c0_v36_142.mp3',
    '../stim/samediff/biru_fast_diff_20_c0_v36_170.mp3',
    '../stim/samediff/biru_fast_diff_20_c0_v36_197.mp3',
    '../stim/samediff/biru_fast_diff_20_c0_v36_260.mp3',
    '../stim/samediff/biru_fast_diff_20_c0_v36_80.mp3',
    '../stim/samediff/biru_fast_same_20_142.mp3',
    '../stim/samediff/biru_fast_same_20_170.mp3',
    '../stim/samediff/biru_fast_same_20_197.mp3',
    '../stim/samediff/biru_fast_same_20_260.mp3',
    '../stim/samediff/biru_fast_same_20_80.mp3',
    '../stim/samediff/biru_slow_diff_20_c0_v36_142.mp3',
    '../stim/samediff/biru_slow_diff_20_c0_v36_170.mp3',
    '../stim/samediff/biru_slow_diff_20_c0_v36_197.mp3',
    '../stim/samediff/biru_slow_diff_20_c0_v36_260.mp3',
    '../stim/samediff/biru_slow_diff_20_c0_v36_80.mp3',
    '../stim/samediff/biru_slow_same_20_142.mp3',
    '../stim/samediff/biru_slow_same_20_170.mp3',
    '../stim/samediff/biru_slow_same_20_197.mp3',
    '../stim/samediff/biru_slow_same_20_260.mp3',
    '../stim/samediff/biru_slow_same_20_80.mp3',
    '../stim/samediff/ika_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/ika_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/ika_fast_diff_20_c0_v36_145.mp3',
    '../stim/samediff/ika_fast_diff_20_c0_v36_184.mp3',
    '../stim/samediff/ika_fast_diff_20_c0_v36_70.mp3',
    '../stim/samediff/ika_fast_same_20_110.mp3',
    '../stim/samediff/ika_fast_same_20_127.mp3',
    '../stim/samediff/ika_fast_same_20_145.mp3',
    '../stim/samediff/ika_fast_same_20_184.mp3',
    '../stim/samediff/ika_fast_same_20_70.mp3',
    '../stim/samediff/ika_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/ika_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/ika_slow_diff_20_c0_v36_145.mp3',
    '../stim/samediff/ika_slow_diff_20_c0_v36_184.mp3',
    '../stim/samediff/ika_slow_diff_20_c0_v36_70.mp3',
    '../stim/samediff/ika_slow_same_20_110.mp3',
    '../stim/samediff/ika_slow_same_20_127.mp3',
    '../stim/samediff/ika_slow_same_20_145.mp3',
    '../stim/samediff/ika_slow_same_20_184.mp3',
    '../stim/samediff/ika_slow_same_20_70.mp3',
    '../stim/samediff/ikka_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/ikka_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/ikka_fast_diff_20_c0_v36_145.mp3',
    '../stim/samediff/ikka_fast_diff_20_c0_v36_184.mp3',
    '../stim/samediff/ikka_fast_diff_20_c0_v36_70.mp3',
    '../stim/samediff/ikka_fast_same_20_110.mp3',
    '../stim/samediff/ikka_fast_same_20_127.mp3',
    '../stim/samediff/ikka_fast_same_20_145.mp3',
    '../stim/samediff/ikka_fast_same_20_184.mp3',
    '../stim/samediff/ikka_fast_same_20_70.mp3',
    '../stim/samediff/ikka_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/ikka_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/ikka_slow_diff_20_c0_v36_145.mp3',
    '../stim/samediff/ikka_slow_diff_20_c0_v36_184.mp3',
    '../stim/samediff/ikka_slow_diff_20_c0_v36_70.mp3',
    '../stim/samediff/ikka_slow_same_20_110.mp3',
    '../stim/samediff/ikka_slow_same_20_127.mp3',
    '../stim/samediff/ikka_slow_same_20_145.mp3',
    '../stim/samediff/ikka_slow_same_20_184.mp3',
    '../stim/samediff/ikka_slow_same_20_70.mp3',
    '../stim/samediff/kaado_fast_diff_20_c0_v36_132.mp3',
    '../stim/samediff/kaado_fast_diff_20_c0_v36_151.mp3',
    '../stim/samediff/kaado_fast_diff_20_c0_v36_169.mp3',
    '../stim/samediff/kaado_fast_diff_20_c0_v36_212.mp3',
    '../stim/samediff/kaado_fast_diff_20_c0_v36_89.mp3',
    '../stim/samediff/kaado_fast_same_20_132.mp3',
    '../stim/samediff/kaado_fast_same_20_151.mp3',
    '../stim/samediff/kaado_fast_same_20_169.mp3',
    '../stim/samediff/kaado_fast_same_20_212.mp3',
    '../stim/samediff/kaado_fast_same_20_89.mp3',
    '../stim/samediff/kaado_slow_diff_20_c0_v36_132.mp3',
    '../stim/samediff/kaado_slow_diff_20_c0_v36_151.mp3',
    '../stim/samediff/kaado_slow_diff_20_c0_v36_169.mp3',
    '../stim/samediff/kaado_slow_diff_20_c0_v36_212.mp3',
    '../stim/samediff/kaado_slow_diff_20_c0_v36_89.mp3',
    '../stim/samediff/kaado_slow_same_20_132.mp3',
    '../stim/samediff/kaado_slow_same_20_151.mp3',
    '../stim/samediff/kaado_slow_same_20_169.mp3',
    '../stim/samediff/kaado_slow_same_20_212.mp3',
    '../stim/samediff/kaado_slow_same_20_89.mp3',
    '../stim/samediff/kado_fast_diff_20_c0_v36_132.mp3',
    '../stim/samediff/kado_fast_diff_20_c0_v36_151.mp3',
    '../stim/samediff/kado_fast_diff_20_c0_v36_169.mp3',
    '../stim/samediff/kado_fast_diff_20_c0_v36_212.mp3',
    '../stim/samediff/kado_fast_diff_20_c0_v36_89.mp3',
    '../stim/samediff/kado_fast_same_20_132.mp3',
    '../stim/samediff/kado_fast_same_20_151.mp3',
    '../stim/samediff/kado_fast_same_20_169.mp3',
    '../stim/samediff/kado_fast_same_20_212.mp3',
    '../stim/samediff/kado_fast_same_20_89.mp3',
    '../stim/samediff/kado_slow_diff_20_c0_v36_132.mp3',
    '../stim/samediff/kado_slow_diff_20_c0_v36_151.mp3',
    '../stim/samediff/kado_slow_diff_20_c0_v36_169.mp3',
    '../stim/samediff/kado_slow_diff_20_c0_v36_212.mp3',
    '../stim/samediff/kado_slow_diff_20_c0_v36_89.mp3',
    '../stim/samediff/kado_slow_same_20_132.mp3',
    '../stim/samediff/kado_slow_same_20_151.mp3',
    '../stim/samediff/kado_slow_same_20_169.mp3',
    '../stim/samediff/kado_slow_same_20_212.mp3',
    '../stim/samediff/kado_slow_same_20_89.mp3',
    '../stim/samediff/kakko_fast_diff_20_c0_v36_109.mp3',
    '../stim/samediff/kakko_fast_diff_20_c0_v36_124.mp3',
    '../stim/samediff/kakko_fast_diff_20_c0_v36_140.mp3',
    '../stim/samediff/kakko_fast_diff_20_c0_v36_174.mp3',
    '../stim/samediff/kakko_fast_diff_20_c0_v36_74.mp3',
    '../stim/samediff/kakko_fast_same_20_109.mp3',
    '../stim/samediff/kakko_fast_same_20_124.mp3',
    '../stim/samediff/kakko_fast_same_20_140.mp3',
    '../stim/samediff/kakko_fast_same_20_174.mp3',
    '../stim/samediff/kakko_fast_same_20_74.mp3',
    '../stim/samediff/kakko_slow_diff_20_c0_v36_109.mp3',
    '../stim/samediff/kakko_slow_diff_20_c0_v36_124.mp3',
    '../stim/samediff/kakko_slow_diff_20_c0_v36_140.mp3',
    '../stim/samediff/kakko_slow_diff_20_c0_v36_174.mp3',
    '../stim/samediff/kakko_slow_diff_20_c0_v36_74.mp3',
    '../stim/samediff/kakko_slow_same_20_109.mp3',
    '../stim/samediff/kakko_slow_same_20_124.mp3',
    '../stim/samediff/kakko_slow_same_20_140.mp3',
    '../stim/samediff/kakko_slow_same_20_174.mp3',
    '../stim/samediff/kakko_slow_same_20_74.mp3',
    '../stim/samediff/kako_fast_diff_20_c0_v36_109.mp3',
    '../stim/samediff/kako_fast_diff_20_c0_v36_124.mp3',
    '../stim/samediff/kako_fast_diff_20_c0_v36_140.mp3',
    '../stim/samediff/kako_fast_diff_20_c0_v36_174.mp3',
    '../stim/samediff/kako_fast_diff_20_c0_v36_74.mp3',
    '../stim/samediff/kako_fast_same_20_109.mp3',
    '../stim/samediff/kako_fast_same_20_124.mp3',
    '../stim/samediff/kako_fast_same_20_140.mp3',
    '../stim/samediff/kako_fast_same_20_174.mp3',
    '../stim/samediff/kako_fast_same_20_74.mp3',
    '../stim/samediff/kako_slow_diff_20_c0_v36_109.mp3',
    '../stim/samediff/kako_slow_diff_20_c0_v36_124.mp3',
    '../stim/samediff/kako_slow_diff_20_c0_v36_140.mp3',
    '../stim/samediff/kako_slow_diff_20_c0_v36_174.mp3',
    '../stim/samediff/kako_slow_diff_20_c0_v36_74.mp3',
    '../stim/samediff/kako_slow_same_20_109.mp3',
    '../stim/samediff/kako_slow_same_20_124.mp3',
    '../stim/samediff/kako_slow_same_20_140.mp3',
    '../stim/samediff/kako_slow_same_20_174.mp3',
    '../stim/samediff/kako_slow_same_20_74.mp3',
    '../stim/samediff/medo_fast_diff_20_c0_v36_148.mp3',
    '../stim/samediff/medo_fast_diff_20_c0_v36_175.mp3',
    '../stim/samediff/medo_fast_diff_20_c0_v36_201.mp3',
    '../stim/samediff/medo_fast_diff_20_c0_v36_261.mp3',
    '../stim/samediff/medo_fast_diff_20_c0_v36_89.mp3',
    '../stim/samediff/medo_fast_same_20_148.mp3',
    '../stim/samediff/medo_fast_same_20_175.mp3',
    '../stim/samediff/medo_fast_same_20_201.mp3',
    '../stim/samediff/medo_fast_same_20_261.mp3',
    '../stim/samediff/medo_fast_same_20_89.mp3',
    '../stim/samediff/medo_slow_diff_20_c0_v36_148.mp3',
    '../stim/samediff/medo_slow_diff_20_c0_v36_175.mp3',
    '../stim/samediff/medo_slow_diff_20_c0_v36_201.mp3',
    '../stim/samediff/medo_slow_diff_20_c0_v36_261.mp3',
    '../stim/samediff/medo_slow_diff_20_c0_v36_89.mp3',
    '../stim/samediff/medo_slow_same_20_148.mp3',
    '../stim/samediff/medo_slow_same_20_175.mp3',
    '../stim/samediff/medo_slow_same_20_201.mp3',
    '../stim/samediff/medo_slow_same_20_261.mp3',
    '../stim/samediff/medo_slow_same_20_89.mp3',
    '../stim/samediff/meido_fast_diff_20_c0_v36_148.mp3',
    '../stim/samediff/meido_fast_diff_20_c0_v36_175.mp3',
    '../stim/samediff/meido_fast_diff_20_c0_v36_201.mp3',
    '../stim/samediff/meido_fast_diff_20_c0_v36_261.mp3',
    '../stim/samediff/meido_fast_diff_20_c0_v36_89.mp3',
    '../stim/samediff/meido_fast_same_20_148.mp3',
    '../stim/samediff/meido_fast_same_20_175.mp3',
    '../stim/samediff/meido_fast_same_20_201.mp3',
    '../stim/samediff/meido_fast_same_20_261.mp3',
    '../stim/samediff/meido_fast_same_20_89.mp3',
    '../stim/samediff/meido_slow_diff_20_c0_v36_148.mp3',
    '../stim/samediff/meido_slow_diff_20_c0_v36_175.mp3',
    '../stim/samediff/meido_slow_diff_20_c0_v36_201.mp3',
    '../stim/samediff/meido_slow_diff_20_c0_v36_261.mp3',
    '../stim/samediff/meido_slow_diff_20_c0_v36_89.mp3',
    '../stim/samediff/meido_slow_same_20_148.mp3',
    '../stim/samediff/meido_slow_same_20_175.mp3',
    '../stim/samediff/meido_slow_same_20_201.mp3',
    '../stim/samediff/meido_slow_same_20_261.mp3',
    '../stim/samediff/meido_slow_same_20_89.mp3',
    '../stim/samediff/meta_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/meta_fast_diff_20_c0_v36_129.mp3',
    '../stim/samediff/meta_fast_diff_20_c0_v36_148.mp3',
    '../stim/samediff/meta_fast_diff_20_c0_v36_191.mp3',
    '../stim/samediff/meta_fast_diff_20_c0_v36_66.mp3',
    '../stim/samediff/meta_fast_same_20_110.mp3',
    '../stim/samediff/meta_fast_same_20_129.mp3',
    '../stim/samediff/meta_fast_same_20_148.mp3',
    '../stim/samediff/meta_fast_same_20_191.mp3',
    '../stim/samediff/meta_fast_same_20_66.mp3',
    '../stim/samediff/meta_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/meta_slow_diff_20_c0_v36_129.mp3',
    '../stim/samediff/meta_slow_diff_20_c0_v36_148.mp3',
    '../stim/samediff/meta_slow_diff_20_c0_v36_191.mp3',
    '../stim/samediff/meta_slow_diff_20_c0_v36_66.mp3',
    '../stim/samediff/meta_slow_same_20_110.mp3',
    '../stim/samediff/meta_slow_same_20_129.mp3',
    '../stim/samediff/meta_slow_same_20_148.mp3',
    '../stim/samediff/meta_slow_same_20_191.mp3',
    '../stim/samediff/meta_slow_same_20_66.mp3',
    '../stim/samediff/metta_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/metta_fast_diff_20_c0_v36_129.mp3',
    '../stim/samediff/metta_fast_diff_20_c0_v36_148.mp3',
    '../stim/samediff/metta_fast_diff_20_c0_v36_191.mp3',
    '../stim/samediff/metta_fast_diff_20_c0_v36_66.mp3',
    '../stim/samediff/metta_fast_same_20_110.mp3',
    '../stim/samediff/metta_fast_same_20_129.mp3',
    '../stim/samediff/metta_fast_same_20_148.mp3',
    '../stim/samediff/metta_fast_same_20_191.mp3',
    '../stim/samediff/metta_fast_same_20_66.mp3',
    '../stim/samediff/metta_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/metta_slow_diff_20_c0_v36_129.mp3',
    '../stim/samediff/metta_slow_diff_20_c0_v36_148.mp3',
    '../stim/samediff/metta_slow_diff_20_c0_v36_191.mp3',
    '../stim/samediff/metta_slow_diff_20_c0_v36_66.mp3',
    '../stim/samediff/metta_slow_same_20_110.mp3',
    '../stim/samediff/metta_slow_same_20_129.mp3',
    '../stim/samediff/metta_slow_same_20_148.mp3',
    '../stim/samediff/metta_slow_same_20_191.mp3',
    '../stim/samediff/metta_slow_same_20_66.mp3',
    '../stim/samediff/tate_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tate_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tate_fast_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tate_fast_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tate_fast_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tate_fast_same_20_110.mp3',
    '../stim/samediff/tate_fast_same_20_127.mp3',
    '../stim/samediff/tate_fast_same_20_144.mp3',
    '../stim/samediff/tate_fast_same_20_183.mp3',
    '../stim/samediff/tate_fast_same_20_72.mp3',
    '../stim/samediff/tate_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tate_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tate_slow_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tate_slow_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tate_slow_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tate_slow_same_20_110.mp3',
    '../stim/samediff/tate_slow_same_20_127.mp3',
    '../stim/samediff/tate_slow_same_20_144.mp3',
    '../stim/samediff/tate_slow_same_20_183.mp3',
    '../stim/samediff/tate_slow_same_20_72.mp3',
    '../stim/samediff/tatte_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tatte_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tatte_fast_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tatte_fast_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tatte_fast_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tatte_fast_same_20_110.mp3',
    '../stim/samediff/tatte_fast_same_20_127.mp3',
    '../stim/samediff/tatte_fast_same_20_144.mp3',
    '../stim/samediff/tatte_fast_same_20_183.mp3',
    '../stim/samediff/tatte_fast_same_20_72.mp3',
    '../stim/samediff/tatte_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tatte_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tatte_slow_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tatte_slow_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tatte_slow_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tatte_slow_same_20_110.mp3',
    '../stim/samediff/tatte_slow_same_20_127.mp3',
    '../stim/samediff/tatte_slow_same_20_144.mp3',
    '../stim/samediff/tatte_slow_same_20_183.mp3',
    '../stim/samediff/tatte_slow_same_20_72.mp3',
    '../stim/samediff/tosho_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tosho_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tosho_fast_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tosho_fast_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tosho_fast_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tosho_fast_same_20_110.mp3',
    '../stim/samediff/tosho_fast_same_20_127.mp3',
    '../stim/samediff/tosho_fast_same_20_144.mp3',
    '../stim/samediff/tosho_fast_same_20_183.mp3',
    '../stim/samediff/tosho_fast_same_20_72.mp3',
    '../stim/samediff/tosho_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tosho_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tosho_slow_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tosho_slow_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tosho_slow_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tosho_slow_same_20_110.mp3',
    '../stim/samediff/tosho_slow_same_20_127.mp3',
    '../stim/samediff/tosho_slow_same_20_144.mp3',
    '../stim/samediff/tosho_slow_same_20_183.mp3',
    '../stim/samediff/tosho_slow_same_20_72.mp3',
    '../stim/samediff/tousho_fast_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tousho_fast_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tousho_fast_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tousho_fast_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tousho_fast_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tousho_fast_same_20_110.mp3',
    '../stim/samediff/tousho_fast_same_20_127.mp3',
    '../stim/samediff/tousho_fast_same_20_144.mp3',
    '../stim/samediff/tousho_fast_same_20_183.mp3',
    '../stim/samediff/tousho_fast_same_20_72.mp3',
    '../stim/samediff/tousho_slow_diff_20_c0_v36_110.mp3',
    '../stim/samediff/tousho_slow_diff_20_c0_v36_127.mp3',
    '../stim/samediff/tousho_slow_diff_20_c0_v36_144.mp3',
    '../stim/samediff/tousho_slow_diff_20_c0_v36_183.mp3',
    '../stim/samediff/tousho_slow_diff_20_c0_v36_72.mp3',
    '../stim/samediff/tousho_slow_same_20_110.mp3',
    '../stim/samediff/tousho_slow_same_20_127.mp3',
    '../stim/samediff/tousho_slow_same_20_144.mp3',
    '../stim/samediff/tousho_slow_same_20_183.mp3',
    '../stim/samediff/tousho_slow_same_20_72.mp3',
];

// save data function

function saveData(name, data){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data_3.php');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({filename: name, filedata: data}));
}

// run jsPsych

jsPsych.init({
    timeline: timeline,
    default_iti: 50,
    preload_audio: audiopaths,
    on_finish: function() {
        if (should_i_save === 1){
            var my_data = jsPsych.data.get().csv();
            var my_data_name = my_id + "_results";
            saveData(my_data_name, my_data);
        }
    }
});
